
import { initialState, appReducer, actions, types } from '../App';

describe( 'Types', ()=> {

    it( 'should have a type to set the system user', ()=> {
        expect( types.APP_SET_USER ).toBe( 'APP_SET_USER' );
    } );
    it( 'should have a type to set the system token', ()=> {
        expect( types.APP_SET_TOKEN ).toBe( 'APP_SET_TOKEN' );
    } );
    it( 'should have a type to set the header-loading indicator', ()=> {
        expect( types.APP_HEADER_SET_IS_LOADING ).toBe( 'APP_HEADER_SET_IS_LOADING' );
    } );
    it( 'should have a type to activate a module', ()=> {
        expect( types.APP_ACTIVATE_MODULE ).toBe( 'APP_ACTIVATE_MODULE' );
    } );
    it( 'should have a type to deactivate a module', ()=> {
        expect( types.APP_DEACTIVATE_MODULE ).toBe( 'APP_DEACTIVATE_MODULE' );
    } );
    it( 'should have a type to open the navbar menu', ()=> {
        expect( types.APP_OPEN_MAIN_MENU ).toBe( 'APP_OPEN_MAIN_MENU' );
    } );
    it( 'should have a type to close the navbar menu', ()=> {
        expect( types.APP_CLOSE_MAIN_MENU ).toBe( 'APP_CLOSE_MAIN_MENU' );
    } );
    it( 'should have a type to open the navbar profile', ()=> {
        expect( types.APP_OPEN_PROFILE ).toBe( 'APP_OPEN_PROFILE' );
    } );
    it( 'should have a type to close the navbar profile', ()=> {
        expect( types.APP_CLOSE_PROFILE ).toBe( 'APP_CLOSE_PROFILE' );
    } );
} );

describe( 'Actions', ()=> {
    const params = { user: {}, token: {}, header: { isLoading: true }, identifier: 'test-module' };

    it( 'should have an action to set the current system user', ()=> {
        const expectedAction = { type: types.APP_SET_USER, user: params.user };
        expect( actions.appSetUser( params.user ) ).toEqual( expectedAction );
    } );
    it( 'should have an action to set the current system token', ()=> {
        const expectedAction = { type: types.APP_SET_TOKEN, token: params.token };
        expect( actions.appSetToken( params.token ) ).toEqual( expectedAction );
    } );
    it( 'should have an action to set the header-loading indicator', ()=> {
        const expectedAction = { type: types.APP_HEADER_SET_IS_LOADING, isLoading: params.header.isLoading };
        expect( actions.appHeaderSetIsLoading( params.header.isLoading ) ).toEqual( expectedAction );
    } );
    it( 'should have an action to activate a module', ()=> {
        const expectedAction = { type: types.APP_ACTIVATE_MODULE, activeModule: params.activeModule };
        expect( actions.activateModule( params.activeModule ) ).toEqual( expectedAction );
    } );
    it( 'should have an action to deactivate a module', ()=> {
        const expectedAction = { type: types.APP_DEACTIVATE_MODULE, activeModule: params.activeModule };
        expect( actions.deactivateModule( params.activeModule ) ).toEqual( expectedAction );
    } );
    it( 'should have an action to open the navbar menu', ()=> {
        const expectedAction = { type: types.APP_OPEN_MAIN_MENU };
        expect( actions.openMenu() ).toEqual( expectedAction );
    } );
    it( 'should have an action to close the navbar menu', ()=> {
        const expectedAction = { type: types.APP_CLOSE_MAIN_MENU };
        expect( actions.closeMenu() ).toEqual( expectedAction );
    } );
    it( 'should have an action to open the navbar profile', ()=> {
        const expectedAction = { type: types.APP_OPEN_PROFILE };
        expect( actions.openProfile() ).toEqual( expectedAction );
    } );
    it( 'should have an action to close the navbar profile', ()=> {
        const expectedAction = { type: types.APP_CLOSE_PROFILE };
        expect( actions.closeProfile() ).toEqual( expectedAction );
    } );
} );

describe( 'InitialState', ()=> {
    let keys = Object.keys( initialState );

    it( 'should have a key for user', ()=> {
        expect( keys.indexOf( 'user' ) ).not.toBe( -1 );
    } );
    it( 'should have a key for token', ()=> {
        expect( keys.indexOf( 'token' ) ).not.toBe( -1 );
    } );
    it( 'should have a key for header', ()=> {
        expect( keys.indexOf( 'header' ) ).not.toBe( -1 );
    } );
    it( 'should have a key for authentication', ()=> {
        expect( keys.indexOf( 'authentication' ) ).not.toBe( -1 );
    } );
    it( 'should have a key for modules', ()=> {
        expect( keys.indexOf( 'modules' ) ).not.toBe( -1 );
    } );
} );

describe( 'Reducer', ()=> {
    it( 'should return the initial state when no state is provided', ()=> {
        const stateToMatch = {
            user: undefined,
            token: undefined,
            header: {},
            authentication: { login: {}, profile: {}, register: {} },
            modules: {}
        };
        const reducerResult = appReducer( undefined, {} );
        expect( reducerResult ).toMatchObject( stateToMatch );
    } );
    it( 'should return a matching state when one is provided', ()=> {
        const stateToMatch = {
            user: { first_name: 'test' },
            token: undefined,
            header: {},
            authentication: { login: {}, profile: {}, register: {} },
            modules: {}
        };
        const reducerResult = appReducer( stateToMatch, {} );
        expect( reducerResult ).toMatchObject( stateToMatch );
    } );
    it( 'should handle setting the current system user', ()=> {
        const reducerResult = appReducer( undefined, { type: types.APP_SET_USER, user: { first_name: 'test' } } );
        expect( reducerResult.user ).toEqual( { first_name: 'test' } );
    } );
    it( 'should handle setting the current system token', ()=> {
        const reducerResult = appReducer( undefined, { type: types.APP_SET_TOKEN, token: {} } );
        expect( reducerResult.token ).toEqual( {} );
    } );
    it( 'should handle showing / hiding the loading-indicator', ()=> {
        const reducerResult = appReducer( undefined, { type: types.APP_HEADER_SET_IS_LOADING, isLoading: true } );
        expect( reducerResult.header.isLoading ).toEqual( true );
    } );
    it( 'should handle showing the header menu', ()=> {
        const reducerResult = appReducer( { ...initialState, header: { menu: { isOpen: false } } }, { type: types.APP_OPEN_MAIN_MENU } );
        expect( reducerResult.header.menu.isOpen ).toEqual( true );
    } );
    it( 'should handle hiding the header menu', ()=> {
        const reducerResult = appReducer( { ...initialState, header: { menu: { isOpen: true } } }, { type: types.APP_CLOSE_MAIN_MENU } );
        expect( reducerResult.header.menu.isOpen ).toEqual( false );
    } );
    it( 'should handle showing the header profile', ()=> {
        const reducerResult = appReducer( { ...initialState, header: { profile: { isOpen: false } } }, { type: types.APP_OPEN_PROFILE } );
        expect( reducerResult.header.profile.isOpen ).toEqual( true );
    } );
    it( 'should handle showing the header profile', ()=> {
        const reducerResult = appReducer( { ...initialState, header: { profile: { isOpen: true } } }, { type: types.APP_CLOSE_PROFILE } );
        expect( reducerResult.header.profile.isOpen ).toEqual( false );
    } );
} );