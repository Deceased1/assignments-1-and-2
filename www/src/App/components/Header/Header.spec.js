
import React from 'react';

import { render, fireEvent, cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';

import { Header } from './index';

const renderWithRouter = (ui, { route='/', history=( createMemoryHistory({ initialEntries: [route] }) ) }={})=> {
    return {
        ...render( <Router history={ history }>{ ui }</Router> ),
        history
    };
};

const defaultProps = (props)=> ({
    title: '2e Systems',
    isLoading: false,
    menu: {
        isOpen: false,
        isVisible: false
    },
    profile: {
        isVisible: false,
        isOpen: false,
        account: undefined,
    },
    openMenu: jest.fn(),
    closeMenu: jest.fn(),
    openProfile: jest.fn(),
    closeProfile: jest.fn(),
    onSignOut: jest.fn(),
    ...props
});

afterEach( cleanup );

describe( 'render', ()=> {
    describe( 'default', ()=> {    
        it( 'should not render the main-menu button', ()=> {
            const props = defaultProps({ menuItems: {} });
            const { queryByLabelText } = renderWithRouter( <Header { ...props } /> );

            const mainMenuButton = queryByLabelText( 'Main Menu Button' );
            expect( mainMenuButton ).not.toBeInTheDocument();
        } );
        it( 'should render the title correctly', ()=> {
            const props = defaultProps({ menuItems: {} });
            const { queryByText } = renderWithRouter( <Header { ...props } /> );
            
            const title = queryByText( props.title );
            expect( title.innerHTML ).toBe( props.title );
        } );
        it( 'should not render the profile-menu button', ()=> {
            const props = defaultProps({ menuItems: {} });
            const { queryByLabelText } = renderWithRouter( <Header { ...props } /> );

            const profileMenuButton = queryByLabelText( 'Profile Menu Button' );
            expect( profileMenuButton ).not.toBeInTheDocument();
        } );
        it( 'should not render the LinearProgress', ()=> {
            const props = defaultProps({ menuItems: {} });
            const { queryByLabelText } = renderWithRouter( <Header { ...props } /> );

            const uploadProgress = queryByLabelText( 'Uploading' );
            expect( uploadProgress ).not.toBeInTheDocument();
        } );
    } );
    
    describe( 'visibility', ()=> {
        it( 'should render the main-menu button', ()=> {
            const props = defaultProps({ menu: { isOpen: false, isVisible: true }, menuItems: {} });
            const { queryByLabelText } = renderWithRouter( <Header { ...props } /> );
    
            const mainMenuButton = queryByLabelText( 'Main Menu Button' );
            expect( mainMenuButton ).toBeInTheDocument();
        } );
        it( 'should render the profile-menu button', ()=> {
            const props = defaultProps({ profile: { isOpen: false, isVisible: true, account: { url: '' } }, menuItems: {} });
            const { queryByLabelText } = renderWithRouter( <Header { ...props } /> );
    
            const profileMenuButton = queryByLabelText( 'Profile Menu Button' );
            expect( profileMenuButton ).toBeInTheDocument();
        } );
        it( 'should render the LinearProgress', ()=> {
            const props = defaultProps({ isLoading: true, menuItems: {} });
            const { queryByLabelText } = renderWithRouter( <Header { ...props } /> );
    
            const uploadProgress = queryByLabelText( 'Uploading' );
            expect( uploadProgress ).toBeInTheDocument();
        } );
    } );
} );

describe( 'interaction', ()=> {
    describe( 'main-menu', ()=> {
        it( 'should call the appropriate function to open the main-menu drawer', ()=> {
            const props = defaultProps({ menu: { isOpen: false, isVisible: true }, menuItems: {} });
            const { queryByLabelText } = renderWithRouter( <Header { ...props } /> );
    
            const mainMenuButton = queryByLabelText( 'Main Menu Button' );
            fireEvent.click( mainMenuButton );

            expect( props.openMenu ).toHaveBeenCalledTimes( 1 );
        } );
        it( 'should open the main-menu drawer', ()=> {
            const props = defaultProps({ menu: { isOpen: true, isVisible: true }, menuItems: {} });
            const { queryByLabelText } = renderWithRouter( <Header { ...props } /> );
    
            const mainMenu = queryByLabelText( 'Main Menu' );
            expect( mainMenu ).toBeInTheDocument();
        } );
        it( 'should call the appropriate function to close the main-menu drawer', ()=> {
            const props = defaultProps({ menu: { isOpen: true, isVisible: true }, menuItems: {} });
            const { queryByLabelText } = renderWithRouter( <Header { ...props } /> );
    
            const mainMenu = queryByLabelText( 'Main Menu' );
            fireEvent.click( mainMenu );

            expect( props.closeMenu ).toHaveBeenCalledTimes( 1 );
        } );
        it( 'should close the main-menu drawer', ()=> {
            const props = defaultProps({ menu: { isOpen: false, isVisible: true }, menuItems: {} });
            const { queryByLabelText } = renderWithRouter( <Header { ...props } /> );
    
            const mainMenu = queryByLabelText( 'Main Menu' );
            expect( mainMenu ).not.toBeInTheDocument();
        } );
        it( 'should display the registered menu-items within the menu', ()=> {
            const props = defaultProps({ menu: { isOpen: true, isVisible: true }, menuItems: { phonebook: { identifier: 'phonebook', name: 'Phonebook', url: '/phonebook' } } });
            const { queryByLabelText } = renderWithRouter( <Header { ...props } /> );
    
            const menuItem = props.menuItems[ 'phonebook' ];
            const menuItemElement = queryByLabelText( `Main Menu Item: ${ menuItem.name }` );
            expect( menuItemElement ).toBeInTheDocument();
        } );
        it( 'should navigate to the associated url of a clicked item', ()=> {
            const props = defaultProps({ menu: { isOpen: true, isVisible: true }, menuItems: { phonebook: { identifier: 'phonebook', name: 'Phonebook', url: '/phonebook' } } });
            const { queryByLabelText, history } = renderWithRouter( <Header { ...props } /> );
    
            const menuItem = props.menuItems[ 'phonebook' ];
            const menuItemElement = queryByLabelText( `Main Menu Item: ${ menuItem.name }` );
            fireEvent.click( menuItemElement );

            expect( history.location.pathname ).toBe( menuItem.url );
        } );
    } );

    describe( 'profile-menu', ()=> {
        /* Given up on testing where/how this menu renders */
        it( 'does not lend itself well to testing', ()=> {
            expect( true ).toBe( true );
        } );
    } );
} );