
import React from 'react';

import { Link } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Drawer from '@material-ui/core/Drawer';

import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import LinearProgress from '@material-ui/core/LinearProgress';

import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';

import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';

const useStyles = makeStyles( (theme)=> ({
    HeaderRoot: {

    },
    HeaderMainMenuButton: {
        marginRight: theme.spacing( 2 ),
        minWidth: '24px',
    },
    HeaderTitle: {
        flexGrow: 1,
    },
    HeaderProfileMenuButton: {

    },
    HeaderLink: {
        textDecoration: 'none',
        color: 'rgba(0, 0, 0, 0.7)'
    },
    HeaderMainMenu: {
        width: 250,
    }
}) );

export const Header = (props)=> {
    const classes = useStyles();

    const {
        title, isLoading,
        menu, menuItems, profile,
        openMenu, closeMenu,
        openProfile, closeProfile,
        onSignOut
    } = props;
    
    return (
        <div className={ classes.HeaderRoot }>
                    
            <AppBar position="static">
                <Toolbar>
                    <div className={ classes.HeaderMainMenuButton }>
                    { ( menu.isVisible === true ) ?
                        <IconButton
                            edge="start"
                            color="inherit"
                            aria-label="Main Menu Button"
                            onClick={ ()=> { openMenu(); } } >
                            <MenuIcon />
                        </IconButton> :
                        null
                    }
                    </div>
                    <Typography variant="h6" className={ classes.HeaderTitle }>
                        { title }
                    </Typography>
                    <div className={ classes.HeaderProfileMenuButton }>
                    { ( profile.isVisible === true ) ? 
                        <div>
                            <IconButton
                                id="profile-menu-button"
                                aria-label="Profile Menu Button"
                                aria-controls="profile-menu"
                                aria-haspopup="true"
                                onClick={ ()=> { openProfile(); } }
                                color="inherit" >
                                <AccountCircle />
                            </IconButton>
                            <Menu keepMounted
                                id="profile-menu"
                                aria-label="Profile Menu"
                                anchorEl={ document.getElementById( "profile-menu-button" ) }
                                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                                open={ profile.isOpen }
                                onClose={ ()=> { closeProfile(); } } >
                                <Link className={ classes.HeaderLink } to={ '/profile' } >
                                    <MenuItem onClick={ ()=> { closeProfile(); } } >
                                        <ListItemIcon>
                                            <Icon >{ 'settings' }</Icon>
                                        </ListItemIcon>
                                        Profile
                                    </MenuItem>
                                </Link>
                                <MenuItem onClick={ ()=> { onSignOut(); } }>
                                    <ListItemIcon>
                                        <Icon >{ 'exit_to_app' }</Icon>
                                    </ListItemIcon>
                                    Sign Out
                                </MenuItem>
                            </Menu>
                        </div> :
                        null
                    }
                    </div>
                </Toolbar>
                { ( ( isLoading === true ) ?
                    <LinearProgress
                        aria-label="Uploading" /> :
                    null
                ) }
            </AppBar>
            
            <Drawer open={ menu.isOpen } onClose={ ()=> { closeMenu(); } } >
                <div
                    className={ classes.HeaderMainMenu }
                    role="presentation"
                    aria-label="Main Menu"
                    onClick={ ()=> { closeMenu(); } }
                    onKeyDown={ ()=> { closeMenu(); } } >
                    <List>
                    { ( Object.values( menuItems ) ).map( (menuItem)=> (
                        <Link
                            className={ classes.HeaderLink }
                            key={ menuItem.identifier }
                            aria-label={ `Main Menu Item: ${ menuItem.name }` }
                            to={ menuItem.url } >
                            <ListItem button >
                                <ListItemIcon>
                                    <Icon>{ menuItem.icon }</Icon>
                                </ListItemIcon>
                                <ListItemText primary={ menuItem.name } />
                            </ListItem>
                        </Link>
                    ) ) }
                    </List>
                </div>
            </Drawer>
        </div>
    );
};