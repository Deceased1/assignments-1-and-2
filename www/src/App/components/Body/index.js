
import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import Container from '@material-ui/core/Container';

import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles( (theme)=> ({
    BodyRoot: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        overflowY: 'auto',
        paddingLeft: '0',
        paddingRight: '0',
    },
    BodyContainer: {
        display: 'inherit',
        backgroundColor: 'transparent',
        flexGrow: 1,
    }
}) );

export const Body = (props)=> {
    const classes = useStyles();

    const {
        children
    } = props;
    
    return (
        <Container
            className={ classes.BodyRoot }
            maxWidth={ false } >
            <Paper
                className={ classes.BodyContainer }
                elevation={ 0 } >
                { children }
            </Paper>
        </Container>
    );
};