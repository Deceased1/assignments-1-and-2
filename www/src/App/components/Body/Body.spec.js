
import React from 'react';

import { render, cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';

import { Body } from './index';

const renderWithRouter = (ui, { route='/', history=( createMemoryHistory({ initialEntries: [route] }) ) }={})=> {
    return {
        ...render( <Router history={ history }>{ ui }</Router> ),
        history
    };
};

const defaultProps = (props)=> ({
    children: ( <div>Test Child</div> ),
    ...props
});

afterEach( cleanup );

describe( 'render', ()=> {
    describe( 'default', ()=> {
        it( 'should render the children passed to it', ()=> {
            const props = defaultProps();
            const { queryByText } = renderWithRouter( <Body { ...props } /> );

            expect( queryByText( 'Test Child' ) ).toBeInTheDocument();
        } );
    } );
} );