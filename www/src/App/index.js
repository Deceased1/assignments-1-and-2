
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/styles';

import { NET } from '../net';

import { Base, styles } from './App.jsx';

import { loginReducer as login } from '../views/Login';
import { registerReducer as register } from '../views/Register';
import { profileReducer as profile } from '../views/Profile';
import { contactsReducer as contacts } from '../views/Contacts';

/* Types */
export const types = {
    APP_OPEN_MAIN_MENU: 'APP_OPEN_MAIN_MENU',
    APP_CLOSE_MAIN_MENU: 'APP_CLOSE_MAIN_MENU',
    APP_OPEN_PROFILE: 'APP_OPEN_PROFILE',
    APP_CLOSE_PROFILE: 'APP_CLOSE_PROFILE',

    APP_ACTIVATE_MODULE: 'APP_ACTIVATE_MODULE',
    APP_DEACTIVATE_MODULE: 'APP_DEACTIVATE_MODULE',

    APP_HEADER_SET_IS_LOADING: 'APP_HEADER_SET_IS_LOADING',

    APP_SET_USER: 'APP_SET_USER',
    APP_SET_TOKEN: 'APP_SET_TOKEN',
};

/* Actions */
export const actions = {
    openMenu: ()=> ({ type: types.APP_OPEN_MAIN_MENU }),
    closeMenu: ()=> ({ type: types.APP_CLOSE_MAIN_MENU }),
    openProfile: ()=> ({ type: types.APP_OPEN_PROFILE }),
    closeProfile: ()=> ({ type: types.APP_CLOSE_PROFILE }),

    activateModule: (identifier)=> ({ type: types.APP_ACTIVATE_MODULE, identifier }),
    deactivateModule: (identifier)=> ({ type: types.APP_DEACTIVATE_MODULE, identifier }),

    appHeaderSetIsLoading: ( isLoading=false )=> ({ type: types.APP_HEADER_SET_IS_LOADING, isLoading }),

    appSetUser: ( user )=> ({ type: types.APP_SET_USER, user }),
    appSetToken: (token)=> ({ type: types.APP_SET_TOKEN, token }),
};

/* Initial State */
export const initialState = {
    user: undefined,
    token: undefined,
    header: {
        title: '2e Systems',
        isLoading: false,
        menu: {
            isOpen: false,
            isVisible: false
        },
        profile: {
            isOpen: false,
            isVisible: false,
            account: undefined,
        },
    },
    authentication: {
        login: undefined,
        register: undefined,
        profile: undefined
    },
    modules: {
        contacts: undefined
    }
};

/* Reducer */
export const appReducer = (state={...initialState}, action)=> {
    let stateReduction;
    switch ( action.type ) {
        case types.APP_OPEN_MAIN_MENU: {
            stateReduction = { ...state,
                header: { ...state.header,
                    menu: { ...state.header.menu,
                        isOpen: true
                    }
                }
            }; break;
        }
        case types.APP_CLOSE_MAIN_MENU: {
            stateReduction = { ...state,
                header: { ...state.header,
                    menu: { ...state.header.menu,
                        isOpen: false
                    }
                }
            }; break;
        }
        case types.APP_OPEN_PROFILE: {
            stateReduction = { ...state,
                header: { ...state.header,
                    profile: { ...state.header.profile,
                        isOpen: true
                    }
                }
            }; break;
        }
        case types.APP_CLOSE_PROFILE: {
            stateReduction = { ...state,
                header: { ...state.header,
                    profile: { ...state.header.profile,
                        isOpen: false
                    }
                }
            }; break;
        }
        case types.APP_HEADER_SET_IS_LOADING: {
            stateReduction = { ...state,
                header: { ...state.header,
                    isLoading: action.isLoading
                }
            }; break;
        }
        case types.APP_SET_USER: {
            stateReduction = { ...state,
                user: action.user,
                header: { ...state.header,
                    menu: { ...state.header.menu,
                        isVisible: ( action.user !== undefined )
                    },
                    profile: { ...state.header.profile,
                        isVisible: ( action.user !== undefined )
                    }
                }
            }; break;
        }
        case types.APP_SET_TOKEN: {
            stateReduction = { ...state,
                token: action.token
            }; break;
        }
        default: stateReduction = { ...state };
    }
    return {
        ...stateReduction,
        authentication: {
            login: login( state.authentication.login, action ),
            register: register( state.authentication.register, action ),
            profile: profile( state.authentication.profile, action )
        },
        modules: {
            contacts: contacts( state.modules.contacts, action ),
        }
    }
};

const mapStateToProps = (state)=> ({
    ...state,
});

const mapDispatchToProps = (dispatch)=> ({
    openMainMenu: ()=> { dispatch( actions.openMenu() ) },
    closeMainMenu: ()=> { dispatch( actions.closeMenu() ) },
    openProfile: ()=> { dispatch( actions.openProfile() ) },
    closeProfile: ()=> { dispatch( actions.closeProfile() ) },

    setUser: (user)=> { dispatch( actions.appSetUser( user ) ); },
    setToken: (token)=> { dispatch( actions.appSetToken( token ) ); },
    
    performLogout: async (token)=> {
        dispatch( actions.appHeaderSetIsLoading( true ) );

        await NET.API_V1.LOGOUT(token);
		localStorage.removeItem( 'Session' );

        dispatch( actions.appSetUser( undefined ) );
        dispatch( actions.appSetToken( undefined ) );

        // Ensure that the profile-menu is closed...
        dispatch( actions.closeProfile() );

        dispatch( actions.appHeaderSetIsLoading( false ) );
    }
});

export { Base as AppComponent };
export const App = ( connect( mapStateToProps, mapDispatchToProps )( withStyles( styles )( Base ) ) );