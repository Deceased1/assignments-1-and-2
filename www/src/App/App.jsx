
import React from 'react';

import { Redirect, Route, Switch } from 'react-router-dom';

import Container from '@material-ui/core/Container';

import { Header } from './components/Header';
import { Body } from './components/Body';

import { Login } from '../views/Login';
import { Register } from '../views/Register';
import { Profile } from '../views/Profile';
import { Contacts } from '../views/Contacts';

export const styles = ()=> ({
    AppRoot: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        paddingLeft: '0',
        paddingRight: '0'
    }
});

/*
 * This component is essentially the 'Shell' of the application... It
 * is the top-most component, which houses all sub-components. It is responsible
 * for displaying the correct 'View' based on routes ( fed from react-router ).
 */
export class Base extends React.PureComponent {

    UNSAFE_componentWillMount() {
        const {
            user, token,
            setUser, setToken
        } = this.props;

        const storedSession = JSON.parse( localStorage.getItem( 'Session' ) );
        if ( (!user) || (!token) ) {
            if ( (storedSession) && (storedSession.user) && (storedSession.token) ) {
                setUser( storedSession.user );
                setToken( storedSession.token );
            }
        }
    }
    
    render() {
        const {
            classes,
            token, header, modules,
            openMainMenu, closeMainMenu,
            openProfile, closeProfile,
            performLogout
        } = this.props;

        return (
            <Container
                className={ classes.AppRoot }
                maxWidth={ false } >
                    
                <Header { ...header }
                    menuItems={ modules }
                    openMenu={ ()=> { openMainMenu() } }
                    closeMenu={ ()=> { closeMainMenu() } }
                    openProfile={ ()=> { openProfile() } }
                    closeProfile={ ()=> { closeProfile() } }
                    onSignOut={ ()=> { performLogout( token ) } } />

                <Body >
                    <Switch>
                        <Route path="/login" render={ (props)=> ( <Login { ...props } /> ) } />
                        <Route path="/register" render={ (props)=> ( <Register { ...props } /> ) } />
                        <Route path="/profile" render={ (props)=> ( <Profile { ...props } /> ) } />
                        <Route path="/contacts" render={ (props)=> ( <Contacts { ...props } /> ) } />
                        <Redirect to='/login' />
                    </Switch>
                </Body>
            </Container>
        );
    }
};