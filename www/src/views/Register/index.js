
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/styles';

import { NET } from '../../net';

import * as app from '../../App';

import { Base, styles } from './Register.jsx';

/* Types */
export const types = {
    
};

/* Actions */
export const actions = {
    
};

/* Initial State */
export const initialState = {
    registerForm: {
        fields: {
            email: '',
            username: '',
            password: '',
            confirm_password: '',
            first_name: '',
            last_name: '',
            date_of_birth: ''
        },
        errors: []
    }
};

/* Reducer */
export const registerReducer = (state={...initialState}, action)=> {
    let stateReduction;
    switch ( action.type ) {
        default: stateReduction = { ...state };
    }
    return {
        ...stateReduction
    }
};

const mapStateToProps = (state)=> ({
    ...state,
    ...state.authentication.register,
});

const mapDispatchToProps = (dispatch)=> ({
    performRegister: async (details)=> {
        dispatch( app.actions.appHeaderSetIsLoading( true ) );

        let user = await NET.API_V1.REGISTER( details );

        let session = await NET.API_V1.LOGIN({ username: details.email, password: details.password });
		localStorage.setItem( 'Session', JSON.stringify( session ) );

        dispatch( app.actions.appSetUser( user ) );
        dispatch( app.actions.appSetToken() );
        
        dispatch( app.actions.appHeaderSetIsLoading( false ) );
    }
});

export { Base as RegisterComponent };
export const Register = ( connect( mapStateToProps, mapDispatchToProps )( withStyles( styles )( Base ) ) );