
import React from 'react';

import { render, fireEvent, cleanup, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';

import { RegisterForm } from './index';

const renderWithRouter = (ui, { route='/', history=( createMemoryHistory({ initialEntries: [route] }) ) }={})=> {
    return {
        ...render( <Router history={ history }>{ ui }</Router> ),
        history
    };
};

const defaultProps = (props)=> ({
    fields: {
        email: '',
        username: '',
        password: '',
        confirm_password: '',
        first_name: '',
        last_name: '',
        date_of_birth: ''
    },
    errors: [],
    onSubmit: jest.fn(),
    ...props
});

afterEach( cleanup );

describe( 'render', ()=> {
    describe( 'default', ()=> {    
        it( 'should render the Register Form', ()=> {
            const props = defaultProps();
            const { getByText } = renderWithRouter( <RegisterForm { ...props } /> );

            const registerForm = getByText( 'Register' );
            expect( registerForm ).toBeInTheDocument();
        } );
        it( 'should enable the Submit button by default', ()=> {
            const props = defaultProps();
            const { getByText } = renderWithRouter( <RegisterForm { ...props } /> );

            const submitButon = getByText( 'Sign Up' );
            expect( submitButon ).not.toHaveAttribute( 'disabled' );
        } );
    } );
} );

describe( 'interaction', ()=> {
    describe( 'touching fields', ()=> {
        it( 'should produce errors for empty fields', async ()=> {
            const props = defaultProps();
            const { getByLabelText, findByText } = renderWithRouter( <RegisterForm { ...props } /> );
    
            const usernameField = getByLabelText( 'Username' );
            
            fireEvent.blur( usernameField );
        
            expect( await findByText( /Please specify a Username/i ) ).not.toBeNull();
        } );
    } );
    describe( 'submitting', ()=> {
        it( 'should call the appropriate function to submit the Register-Request', async ()=> {
            const props = defaultProps({ fields: {
                email: 'user@test.com',
                username: 'test-user',
                password: 'password',
                confirm_password: 'password', 
                first_name: 'test',
                last_name: 'user',
                date_of_birth: '01-01-1970T00:00:00'
            } });
            const { getByText } = renderWithRouter( <RegisterForm { ...props } /> );
    
            const submitButon = getByText( 'Sign Up' );
            fireEvent.click( submitButon );
            
            /* Use the 'wait' api for Formik submits */
            await wait( ()=> {
                expect( props.onSubmit ).toHaveBeenCalledTimes( 1 );
            } );
        } );
    } );
} );