
import React from 'react';

import { Link } from 'react-router-dom';

import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import { makeStyles } from '@material-ui/core/styles';

import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Snackbar from '@material-ui/core/Snackbar';
import Typography from '@material-ui/core/Typography';

import { TextField } from 'formik-material-ui';

const useStyles = makeStyles( (theme)=> ({
    RegisterFormRoot: {
        alignItems: 'center',
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'center',
    },
    FormContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    Form: {
        marginTop: theme.spacing( 1 ),
        width: '100%',
    },
    Submit: {
        margin: theme.spacing( 3, 0, 2 ),
    },
    RegisterLink: {
        color: '#FFFFFF',
        textDecoration: 'none',
    },
}) );

const validationSchema = Yup.object().shape({
    email: Yup.string()
        .ensure()
        .required( 'Please specify an Email Address' ),
    username: Yup.string()
        .ensure()
        .required( 'Please specify a Username' ),
    password: Yup.string()
        .ensure()
        .required( 'Please specify a Password' ),
    confirm_password: Yup.string()
        .ensure()
        .required( "Please Confirm your Password" )
        .oneOf([ Yup.ref( "password" ), null ], "Passwords must match" ),
    first_name: Yup.string()
        .ensure()
        .required( 'Please specify your First Name' ),
    last_name: Yup.string()
        .ensure()
        .required( 'Please specify your Last Name' ),
    date_of_birth: Yup.string()
        .ensure()
        .required( 'Please specify your Date of Birth' )
});

export const RegisterForm = (props)=> {
    const classes = useStyles();

    const {
        fields, errors,
        onSubmit
    } = props;
    
    return (
        <div className={ classes.RegisterFormRoot }>
            <Formik
                initialValues={ fields }
                validationSchema={ validationSchema }
                onSubmit={ (values, bag )=> {
                    onSubmit( values, bag );
                } } >
                { ({ submitForm, isSubmitting, values, errors })=> (
                    <Container className={ classes.FormContainer } maxWidth='xs'>
                        <Card>
                            <CardContent>
                                <Typography variant="h4" gutterBottom>
                                    Register
                                </Typography>
                                <Typography variant="caption" gutterBottom>
                                    Sign Up with details that uniquely identifies your account
                                </Typography>
                                <Form className={ classes.Form } >
                                    <Field fullWidth
                                        component={ TextField }
                                        id='email'
                                        name='email'
                                        label='Email'
                                        type='email'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='email' />

                                    <Field fullWidth
                                        component={ TextField }
                                        id='username'
                                        name='username'
                                        label='Username'
                                        type='text'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='username' />

                                    <Field fullWidth
                                        component={ TextField }
                                        id='password'
                                        name='password'
                                        label='Password'
                                        type='password'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='password' />

                                    <Field fullWidth
                                        component={ TextField }
                                        id='confirm_password'
                                        name='confirm_password'
                                        label='Confirm Password'
                                        type='password'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='confirm_password' />

                                    <Field fullWidth
                                        component={ TextField }
                                        id='first_name'
                                        name='first_name'
                                        label='First Name'
                                        type='text'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='first_name' />

                                    <Field fullWidth
                                        component={ TextField }
                                        id='last_name'
                                        name='last_name'
                                        label='Last Name'
                                        type='text'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='last_name' />

                                    <Field fullWidth
                                        component={ TextField }
                                        id='date_of_birth'
                                        name='date_of_birth'
                                        label='Date of Birth'
                                        type='text'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='date_of_birth' />
                                        
                                    <Button fullWidth 
                                        className={ classes.Submit }
                                        type='submit'
                                        variant='contained'
                                        color='primary'
                                        disabled={ ( isSubmitting || ( ( Object.values( errors ) ).length > 0 ) ) }
                                        onClick={ submitForm } >
                                        Sign Up
                                    </Button>
                                        
                                    <Button fullWidth 
                                        variant='contained'
                                        color='secondary' >
                                        <Link className={ classes.RegisterLink } to={ '/login' } >
                                            Already have an account?
                                        </Link>
                                    </Button>
                                    
                                </Form>
                            </CardContent>
                        </Card>
                    </Container>
                ) }
            </Formik>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={ ( (errors[ 0 ] !== undefined) ? true : false ) }
                autoHideDuration={ 6000 }
                message={
                    <Typography variant='body1' color='error'>
                        { ( (errors[ 0 ]) ? errors[ 0 ] : null ) }
                    </Typography>
                } />
        </div>
    );
};