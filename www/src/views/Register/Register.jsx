
import React from 'react';

import { Redirect } from 'react-router-dom';

import Container from '@material-ui/core/Container';

import { RegisterForm } from './components/RegisterForm';

export const styles = ()=> ({
    RegisterRoot: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        paddingLeft: '0',
        paddingRight: '0'
    }
});

/*
 * This component hosts the interface for registering on the application.
 */
export class Base extends React.PureComponent {
    
    render() {
        const {
            classes,
            user, registerForm,
            performRegister
        } = this.props;

        return (
            <Container
                className={ classes.RegisterRoot }
                maxWidth={ false } >
                { ( (!user) ?
                    <RegisterForm { ...registerForm } onSubmit={ performRegister } /> :
                    <Redirect to={ '/profile' } />
                ) }
            </Container>
        );
    }
};