
import { initialState, registerReducer, actions, types } from '../Register';

describe.skip( 'Types', ()=> {
    
} );

describe.skip( 'Actions', ()=> {
    const params = {  };
} );

describe( 'InitialState', ()=> {
    let keys = Object.keys( initialState );

    it( 'should have a key for registerForm', ()=> {
        expect( keys.indexOf( 'registerForm' ) ).not.toBe( -1 );
    } );
} );

describe( 'Reducer', ()=> {
    it( 'should return the initial state when no state is provided', ()=> {
        const stateToMatch = { ...initialState };
        const reducerResult = registerReducer( undefined, {} );
        expect( reducerResult ).toMatchObject( stateToMatch );
    } );
    it( 'should return a matching state when one is provided', ()=> {
        const stateToMatch = { ...initialState, hasErrors: true };
        const reducerResult = registerReducer( stateToMatch, {} );
        expect( reducerResult ).toMatchObject( stateToMatch );
    } );
} );