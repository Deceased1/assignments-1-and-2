
import { initialState, loginReducer, actions, types } from '../Login';

describe( 'Types', ()=> {

    it( 'should have a type to set the has-errors flag', ()=> {
        expect( types.LOGIN_SET_HAS_ERRORS ).toBe( 'LOGIN_SET_HAS_ERRORS' );
    } );
    it( 'should have a type to set errors', ()=> {
        expect( types.LOGIN_SET_ERRORS ).toBe( 'LOGIN_SET_ERRORS' );
    } );
} );

describe( 'Actions', ()=> {
    const params = { hasErrors: false, errors: [ 'test-error' ] };

    it( 'should have an action to set the has-errors flag', ()=> {
        const expectedAction = { type: types.LOGIN_SET_HAS_ERRORS, hasErrors: false };
        expect( actions.loginSetHasErrors( params.hasErrors ) ).toEqual( expectedAction );
    } );
    it( 'should have an action to set errors', ()=> {
        const expectedAction = { type: types.LOGIN_SET_ERRORS, errors: [ 'test-error' ] };
        expect( actions.loginSetErrors( params.errors ) ).toEqual( expectedAction );
    } );
} );

describe( 'InitialState', ()=> {
    let keys = Object.keys( initialState );

    it( 'should have a key for loginForm', ()=> {
        expect( keys.indexOf( 'loginForm' ) ).not.toBe( -1 );
    } );
} );

describe( 'Reducer', ()=> {
    it( 'should return the initial state when no state is provided', ()=> {
        const stateToMatch = { ...initialState };
        const reducerResult = loginReducer( undefined, {} );
        expect( reducerResult ).toMatchObject( stateToMatch );
    } );
    it( 'should return a matching state when one is provided', ()=> {
        const stateToMatch = { ...initialState, hasErrors: true };
        const reducerResult = loginReducer( stateToMatch, {} );
        expect( reducerResult ).toMatchObject( stateToMatch );
    } );
    it( 'should handle setting the has-errors flag', ()=> {
        const reducerResult = loginReducer( undefined, { type: types.LOGIN_SET_HAS_ERRORS, hasErrors: true } );
        expect( reducerResult.hasErrors ).toEqual( true );
    } );
    it( 'should handle setting the errors', ()=> {
        const reducerResult = loginReducer( undefined, { type: types.LOGIN_SET_ERRORS, errors: [] } );
        expect( reducerResult.errors ).toEqual( [] );
    } );
} );