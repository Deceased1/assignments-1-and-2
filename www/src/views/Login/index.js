
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/styles';

import { NET } from '../../net';

import * as app from '../../App';

import { Base, styles } from './Login.jsx';

/* Types */
export const types = {
    LOGIN_SET_HAS_ERRORS: 'LOGIN_SET_HAS_ERRORS',
    LOGIN_SET_ERRORS: 'LOGIN_SET_ERRORS'
};

/* Actions */
export const actions = {
    loginSetHasErrors: (hasErrors)=> ({ type: types.LOGIN_SET_HAS_ERRORS, hasErrors }),
    loginSetErrors: (errors)=> ({ type: types.LOGIN_SET_ERRORS, errors })
};

/* Initial State */
export const initialState = {
    loginForm: {
        fields: {
            username: '',
            password: ''
        },
        hasErrors: false,
        errors: []
    }
};

/* Reducer */
export const loginReducer = (state={...initialState}, action)=> {
    let stateReduction;
    switch ( action.type ) {
        case types.LOGIN_SET_HAS_ERRORS: {
            stateReduction = { ...state,
                hasErrors: action.hasErrors
            }; break;
        }
        case types.LOGIN_SET_ERRORS: {
            stateReduction = { ...state,
                errors: action.errors
            }; break;
        }
        default: stateReduction = { ...state };
    }
    return {
        ...stateReduction
    }
};

const mapStateToProps = (state)=> ({
    ...state,
    ...state.authentication.login,
});

const mapDispatchToProps = (dispatch)=> ({
    performLogin: async (details)=> {
        dispatch( app.actions.appHeaderSetIsLoading( true ) );

        let session = await NET.API_V1.LOGIN( details );
		localStorage.setItem( 'Session', JSON.stringify( session ) );

        dispatch( app.actions.appSetUser( session.user ) );
        dispatch( app.actions.appSetToken( session.token ) );
        
        dispatch( app.actions.appHeaderSetIsLoading( false ) );
    }
});

export { Base as LoginComponent };
export const Login = ( connect( mapStateToProps, mapDispatchToProps )( withStyles( styles )( Base ) ) );