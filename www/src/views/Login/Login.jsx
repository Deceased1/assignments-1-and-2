
import React from 'react';

import { Redirect } from 'react-router-dom';

import Container from '@material-ui/core/Container';

import { LoginForm } from './components/LoginForm';

export const styles = ()=> ({
    LoginRoot: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        paddingLeft: '0',
        paddingRight: '0'
    }
});

/*
 * This component hosts the interface for signing into the application.
 */
export class Base extends React.PureComponent {
    
    render() {
        const {
            classes,
            user, loginForm,
            performLogin
        } = this.props;

        return (
            <Container
                className={ classes.LoginRoot }
                maxWidth={ false } >
                { ( (!user) ?
                    <LoginForm { ...loginForm } onSubmit={ performLogin } /> :
                    <Redirect to={ '/profile' } />
                ) }
            </Container>
        );
    }
};