
import React from 'react';

import { render, fireEvent, cleanup, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';

import { LoginForm } from './index';

const renderWithRouter = (ui, { route='/', history=( createMemoryHistory({ initialEntries: [route] }) ) }={})=> {
    return {
        ...render( <Router history={ history }>{ ui }</Router> ),
        history
    };
};

const defaultProps = (props)=> ({
    fields: {
        username: '',
        password: ''
    },
    errors: [],
    onSubmit: jest.fn(),
    ...props
});

afterEach( cleanup );

describe( 'render', ()=> {
    describe( 'default', ()=> {    
        it( 'should render the Login Form', ()=> {
            const props = defaultProps();
            const { getByText } = renderWithRouter( <LoginForm { ...props } /> );

            const loginForm = getByText( 'Login' );
            expect( loginForm ).toBeInTheDocument();
        } );
        it( 'should enable the Submit button by default', ()=> {
            const props = defaultProps();
            const { getByText } = renderWithRouter( <LoginForm { ...props } /> );

            const submitButon = getByText( 'Sign In' );
            expect( submitButon ).not.toHaveAttribute( 'disabled' );
        } );
    } );
} );

describe( 'interaction', ()=> {
    describe( 'touching fields', ()=> {
        it( 'should produce errors for empty fields', async ()=> {
            const props = defaultProps();
            const { getByLabelText, findByText } = renderWithRouter( <LoginForm { ...props } /> );
    
            const usernameField = getByLabelText( 'Username' );
            
            fireEvent.blur( usernameField );
        
            expect( await findByText( /Please specify a Username/i ) ).not.toBeNull();
        } );
    } );
    describe( 'submitting', ()=> {
        it( 'should call the appropriate function to submit the Login-Request', async ()=> {
            const props = defaultProps({ fields: { username: 'test', password: 'password' } });
            const { getByText, debug } = renderWithRouter( <LoginForm { ...props } /> );
    
            const submitButon = getByText( 'Sign In' );
            fireEvent.click( submitButon );
            
            /* Use the 'wait' api for Formik submits */
            await wait( ()=> {
                expect( props.onSubmit ).toHaveBeenCalledTimes( 1 );
            } );
        } );
    } );
} );