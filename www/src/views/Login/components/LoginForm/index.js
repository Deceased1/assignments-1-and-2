
import React from 'react';

import { Link } from 'react-router-dom';

import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import { makeStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Snackbar from '@material-ui/core/Snackbar';
import Typography from '@material-ui/core/Typography';

import { TextField } from 'formik-material-ui';

const useStyles = makeStyles( (theme)=> ({
    LoginFormRoot: {
        alignItems: 'center',
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'center',
    },
    FormContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    Form: {
        marginTop: theme.spacing( 1 ),
        width: '100%',
    },
    Submit: {
        margin: theme.spacing( 3, 0, 2 ),
    },
    RegisterLink: {
        color: '#FFFFFF',
        textDecoration: 'none',
    },
}) );

const validationSchema = Yup.object().shape({
    username: Yup.string()
        .ensure()
        .required( 'Please specify a Username' ),
    password: Yup.string()
        .ensure()
        .required( 'Please specify a Password' )
});

export const LoginForm = (props)=> {
    const classes = useStyles();

    const {
        fields, errors,
        onSubmit
    } = props;
    
    return (
        <div className={ classes.LoginFormRoot }>
            <Formik
                initialValues={ fields }
                validationSchema={ validationSchema }
                onSubmit={ (values, bag )=> {
                    onSubmit( values, bag );
                } } >
                { ({ submitForm, isSubmitting, values, errors })=> (
                    <Container className={ classes.FormContainer } maxWidth='xs'>
                        <Card>
                            <CardContent>
                                <Typography variant="h4" gutterBottom>
                                    Login
                                </Typography>
                                <Typography variant="caption" gutterBottom>
                                    Sign in with your Username or Email address
                                </Typography>
                                <Form className={ classes.Form } >
                                    <Field fullWidth
                                        component={ TextField }
                                        id='username'
                                        name='username'
                                        label='Username'
                                        type='text'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='username' />

                                    <Field fullWidth
                                        component={ TextField }
                                        id='password'
                                        name='password'
                                        label='Password'
                                        type='password'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='password' />
                                        
                                    <Button fullWidth 
                                        className={ classes.Submit }
                                        type='submit'
                                        variant='contained'
                                        color='primary'
                                        disabled={ ( isSubmitting || ( ( Object.values( errors ) ).length > 0 ) ) }
                                        onClick={ submitForm } >
                                        Sign In
                                    </Button>
                                        
                                    <Button fullWidth 
                                        variant='contained'
                                        color='secondary' >
                                        <Link className={ classes.RegisterLink } to={ '/register' } >
                                            Don't have an account?
                                        </Link>
                                    </Button>
                                    
                                </Form>
                            </CardContent>
                        </Card>
                    </Container>
                ) }
            </Formik>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={ ( (errors[ 0 ] !== undefined) ? true : false ) }
                autoHideDuration={ 6000 }
                message={
                    <Typography variant='body1' color='error'>
                        { ( (errors[ 0 ]) ? errors[ 0 ] : null ) }
                    </Typography>
                } />
        </div>
    );
};