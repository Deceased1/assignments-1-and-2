
import { initialState, contactsReducer, actions, types } from '../Contacts';

describe( 'Types', ()=> {

    it( 'should have a type to set the contacts', ()=> {
        expect( types.CONTACTS_SET_CONTACTS ).toBe( 'CONTACTS_SET_CONTACTS' );
    } );
    it( 'should have a type to set the contact-details-form title', ()=> {
        expect( types.CONTACTS_SET_FORM_TITLE ).toBe( 'CONTACTS_SET_FORM_TITLE' );
    } );
    it( 'should have a type to set whether the contact-details-form is visible', ()=> {
        expect( types.CONTACTS_SET_FORM_IS_OPEN ).toBe( 'CONTACTS_SET_FORM_IS_OPEN' );
    } );
    it( 'should have a type to set the fields of the contact-details-form', ()=> {
        expect( types.CONTACTS_SET_FORM_FIELDS ).toBe( 'CONTACTS_SET_FORM_FIELDS' );
    } );
    it( 'should have a type to set whether the delete-contact-dialog is visible', ()=> {
        expect( types.CONTACTS_SET_DIALOG_IS_OPEN ).toBe( 'CONTACTS_SET_DIALOG_IS_OPEN' );
    } );
    it( 'should have a type to set the target contact for the delete-contact-dialog', ()=> {
        expect( types.CONTACTS_SET_DIALOG_CONTACT ).toBe( 'CONTACTS_SET_DIALOG_CONTACT' );
    } );
} );

describe( 'Actions', ()=> {
    const params = { contacts: [], form: { title: 'test-title', isOpen: true, fields: { test: 'field' } }, dialog: { isOpen: true, contact: { first_name: 'test' } } };

    it( 'should have an action to set the contacts', ()=> {
        const expectedAction = { type: types.CONTACTS_SET_CONTACTS, contacts: params.contacts };
        expect( actions.contactsSetContacts( params.contacts ) ).toEqual( expectedAction );
    } );
    it( 'should have an action to set the contact-details-form title', ()=> {
        const expectedAction = { type: types.CONTACTS_SET_FORM_TITLE, title: params.form.title };
        expect( actions.contactsSetFormTitle( params.form.title ) ).toEqual( expectedAction );
    } );
    it( 'should have an action to set whether the contact-details-form is visible', ()=> {
        const expectedAction = { type: types.CONTACTS_SET_FORM_IS_OPEN, isOpen: params.form.isOpen };
        expect( actions.contactsSetFormIsOpen( params.form.isOpen ) ).toEqual( expectedAction );
    } );
    it( 'should have an action to set the fields of the contact-details-form', ()=> {
        const expectedAction = { type: types.CONTACTS_SET_FORM_FIELDS, fields: params.form.fields };
        expect( actions.contactsSetFormFields( params.form.fields ) ).toEqual( expectedAction );
    } );
    it( 'should have an action to set whether the delete-contact-dialog is visible', ()=> {
        const expectedAction = { type: types.CONTACTS_SET_DIALOG_IS_OPEN, isOpen: params.dialog.isOpen };
        expect( actions.contactsSetDialogIsOpen( params.dialog.isOpen ) ).toEqual( expectedAction );
    } );
    it( 'should have an action to set the target contact for the delete-contact-dialog', ()=> {
        const expectedAction = { type: types.CONTACTS_SET_DIALOG_CONTACT, contact: params.dialog.contact };
        expect( actions.contactsSetDialogContact( params.dialog.contact ) ).toEqual( expectedAction );
    } );
} );

describe( 'InitialState', ()=> {
    let keys = Object.keys( initialState );

    it( 'should have a key for identifier', ()=> {
        expect( keys.indexOf( 'identifier' ) ).not.toBe( -1 );
    } );
    it( 'should have a key for icon', ()=> {
        expect( keys.indexOf( 'icon' ) ).not.toBe( -1 );
    } );
    it( 'should have a key for name', ()=> {
        expect( keys.indexOf( 'name' ) ).not.toBe( -1 );
    } );
    it( 'should have a key for url', ()=> {
        expect( keys.indexOf( 'url' ) ).not.toBe( -1 );
    } );
    it( 'should have a key for contacts', ()=> {
        expect( keys.indexOf( 'contacts' ) ).not.toBe( -1 );
    } );
    it( 'should have a key for contactsForm', ()=> {
        expect( keys.indexOf( 'contactsForm' ) ).not.toBe( -1 );
    } );
    it( 'should have a key for contactsDialog', ()=> {
        expect( keys.indexOf( 'contactsDialog' ) ).not.toBe( -1 );
    } );
} );

describe( 'Reducer', ()=> {
    it( 'should return the initial state when no state is provided', ()=> {
        const stateToMatch = {
            identifier: 'contacts',
            icon: 'contacts',
            name: 'Contacts',
            url: '/contacts',
            contacts: [],
            contactsForm: { title: '', isOpen: false, fields: { first_name: '', last_name: '', work: '', phone: '', email: '' }, errors: [] },
            contactsDialog: { isOpen: false, contact: { first_name: '', last_name: '' } }
        };
        const reducerResult = contactsReducer( undefined, {} );
        expect( reducerResult ).toMatchObject( stateToMatch );
    } );
    it( 'should return a matching state when one is provided', ()=> {
        const stateToMatch = {
            identifier: 'contacts',
            icon: 'contacts',
            name: 'Contacts',
            url: '/contacts',
            contacts: [],
            contactsForm: { title: '', isOpen: false, fields: { first_name: '', last_name: '', work: '', phone: '', email: '' }, errors: [] },
            contactsDialog: { isOpen: false, contact: { first_name: '', last_name: '' } }
        };
        const reducerResult = contactsReducer( stateToMatch, {} );
        expect( reducerResult ).toMatchObject( stateToMatch );
    } );
    it( 'should handle setting the contacts', ()=> {
        const reducerResult = contactsReducer( undefined, { type: types.CONTACTS_SET_CONTACTS, contacts: [{ id: 'test' }] } );
        expect( reducerResult.contacts.length ).toEqual( 1 );
    } );
    it( 'should handle setting the contact-details-form title', ()=> {
        const reducerResult = contactsReducer( undefined, { type: types.CONTACTS_SET_FORM_TITLE, title: 'test' } );
        expect( reducerResult.contactsForm.title ).toEqual( 'test' );
    } );
    it( 'should handle showing / hiding contact-details-form', ()=> {
        const reducerResult = contactsReducer( undefined, { type: types.CONTACTS_SET_FORM_IS_OPEN, isOpen: true } );
        expect( reducerResult.contactsForm.isOpen ).toEqual( true );
    } );
    it( 'should handle populating the contact-details-form', ()=> {
        const reducerResult = contactsReducer( undefined, { type: types.CONTACTS_SET_FORM_FIELDS, fields: { first_name: 'test' } } );
        expect( reducerResult.contactsForm.fields.first_name ).toEqual( 'test' );
    } );
    it( 'should handle showing / hiding delete-contact-dialog', ()=> {
        const reducerResult = contactsReducer( undefined, { type: types.CONTACTS_SET_DIALOG_IS_OPEN, isOpen: true } );
        expect( reducerResult.contactsDialog.isOpen ).toEqual( true );
    } );
    it( 'should handle setting the target contact for the delete-contact-dialog', ()=> {
        const reducerResult = contactsReducer( undefined, { type: types.CONTACTS_SET_DIALOG_CONTACT, contact: { first_name: 'test' } } );
        expect( reducerResult.contactsDialog.contact.first_name ).toEqual( 'test' );
    } );
} );