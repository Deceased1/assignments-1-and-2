
import React from 'react';

import { Redirect } from 'react-router-dom';

import Container from '@material-ui/core/Container';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { ContactsList } from './components/ContactsList';
import { ContactDetailsForm } from './components/ContactDetailsForm';

export const styles = (theme)=> ({
    ContactsRoot: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        padding: theme.spacing( 1 )
    }
});

/*
 * This component hosts the interface for signing into the application.
 */
export class Base extends React.PureComponent {

    componentDidMount() {
        const { identifier, token, activateModule, fetchContacts } = this.props;

        activateModule( identifier );

        fetchContacts( token );
    }
    
    render() {
        const {
            classes,
            user, token, contacts,
            contactsForm, openForm,
            contactsDialog, openDialog, closeDialog,
            onAddContact, onEditContact, resetForm,
            onRemoveContact
        } = this.props;

        if ( !user ) {
            return ( <Redirect to={ '/login' } /> );
        }

        return (
            <Container
                className={ classes.ContactsRoot }
                maxWidth={ false } >
                
                <ContactsList
                    data={ contacts }
                    onCreate={ ()=> { openForm( 'Add Contact' ); } }
                    onEdit={ (_, target)=> { openForm( 'Edit Contact', target ); } }
                    onDelete={ (_, target)=> { openDialog( target ); } } />
                
                <Drawer anchor='right' open={ contactsForm.isOpen } onClose={ resetForm } >
                    
                    <ContactDetailsForm { ...contactsForm }
                        onCancel={ resetForm }
                        onSubmit={ (values, _, mode)=> {
                            values.user_fk = user.id;
                            ( (mode === 'create') ?
                            onAddContact( values, token ) : 
                            onEditContact( values, token ) )
                        } } />
    
                </Drawer>

                <Dialog
                    open={ contactsDialog.isOpen } >

                    <DialogTitle id='alert-dialog-title'>
                        { 'Remove Contact' }
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id='alert-dialog-description'>
                            Are you sure that you want to remove contact: <br />
                            <strong>{`${ contactsDialog.contact.first_name } ${ contactsDialog.contact.last_name }`}</strong>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={ closeDialog } color='secondary'>
                            No
                        </Button>
                        <Button onClick={ ()=> { onRemoveContact( contactsDialog.contact, token ); } } color='primary' autoFocus>
                            Yes
                        </Button>
                    </DialogActions>
                </Dialog>
            </Container>
        );
    }

    componentWillUnmount() {
        const { identifier, deactivateModule } = this.props;

        deactivateModule( identifier );
    }
};