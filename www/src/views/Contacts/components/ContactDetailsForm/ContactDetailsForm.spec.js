
import React from 'react';

import { render, fireEvent, cleanup, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';

import { ContactDetailsForm } from './index';

const renderWithRouter = (ui, { route='/', history=( createMemoryHistory({ initialEntries: [route] }) ) }={})=> {
    return {
        ...render( <Router history={ history }>{ ui }</Router> ),
        history
    };
};

const defaultProps = (props)=> ({
    title: 'contact-details',
    isOpen: false,
    fields: {
        first_name: '',
        last_name: '',
        work: '',
        phone: '',
        email: ''
    },
    errors: [],
    onCancel: jest.fn(),
    onSubmit: jest.fn(),
    ...props
});

afterEach( cleanup );

describe( 'render', ()=> {
    describe( 'default', ()=> {    
        it( 'should render the Contact Details Form', ()=> {
            const props = defaultProps();
            const { getByText } = renderWithRouter( <ContactDetailsForm { ...props } /> );

            const contactDetailsForm = getByText( props.title );
            expect( contactDetailsForm ).toBeInTheDocument();
        } );
        it( 'should enable the Submit button by default', ()=> {
            const props = defaultProps();
            const { getByText } = renderWithRouter( <ContactDetailsForm { ...props } /> );

            const submitButton = getByText( 'Save' );
            expect( submitButton ).not.toHaveAttribute( 'disabled' );
        } );
    } );
} );

describe( 'interaction', ()=> {
    describe( 'touching fields', ()=> {
        it( 'should produce errors for empty fields', async ()=> {
            const props = defaultProps();
            const { getByLabelText, findByText } = renderWithRouter( <ContactDetailsForm { ...props } /> );
    
            const firstNameField = getByLabelText( 'First Name' );
            
            fireEvent.blur( firstNameField );
            
            await wait( ()=> {
                expect( findByText( /Please specify the contacts' First Name/i ) ).not.toBeNull();
            } );
        } );
    } );
    describe( 'closing the form', ()=> {
        it( 'should call the appropriate function to submit the ContactDetails-Request', async ()=> {
            const props = defaultProps();
            const { getByLabelText } = renderWithRouter( <ContactDetailsForm { ...props } /> );
    
            const cancelButton = getByLabelText( 'Cancel' );
            fireEvent.click( cancelButton );
            
            /* Use the 'wait' api for Formik submits */
            await wait( ()=> {
                expect( props.onCancel ).toHaveBeenCalledTimes( 1 );
            } );
        } );
    } );
    describe( 'submitting', ()=> {
        it( 'should call the appropriate function to submit the ContactDetails-Request', async ()=> {
            const props = defaultProps({ fields: {
                first_name: 'test',
                last_name: 'contact',
                work: 'tester',
                phone: '0000000000',
                email: 'tuser@test.com'
            } });
            const { getByText } = renderWithRouter( <ContactDetailsForm { ...props } /> );
    
            const submitButton = getByText( 'Save' );
            fireEvent.click( submitButton );
            
            /* Use the 'wait' api for Formik submits */
            await wait( ()=> {
                expect( props.onSubmit ).toHaveBeenCalledTimes( 1 );
            } );
        } );
    } );
} );