
import React from 'react';

import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import { makeStyles } from '@material-ui/core/styles';

import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Typography from '@material-ui/core/Typography';

import { TextField } from 'formik-material-ui';

const useStyles = makeStyles( (theme)=> ({
	ContactDetailsFormContainer: {
        padding: theme.spacing( 0 )
	},
    FormContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: theme.spacing( 1 )
    },
    Form: {
        marginTop: theme.spacing( 1 ),
        width: '100%',
    },
    FormCardHeading: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    FormCardTitle: {
        marginTop: theme.spacing( 1 )
    },
    Submit: {
        margin: theme.spacing( 3, 0, 2 ),
    },
}) );

const validationSchema = Yup.object().shape({
    first_name: Yup.string()
        .ensure()
        .required( 'Please specify the contacts\' First Name' ),
    last_name: Yup.string()
        .ensure()
        .required( 'Please specify the contacts\' Last Name' ),
    work: Yup.string()
        .ensure(),
    phone: Yup.string()
        .ensure(),
    email: Yup.string()
        .ensure()
});

export const ContactDetailsForm = (props)=> {
	const {
        title, fields,
        onCancel, onSubmit
    } = props;

	const classes = useStyles();

	return (
		<Container
			className={ classes.ContactDetailsFormContainer }
			maxWidth={ false } >
                
            <Formik
                initialValues={ fields }
                validateOnMount={ false }
                validationSchema={ validationSchema }
                enableReinitialize={ true }
                onSubmit={ (values, bag )=> {
                    let mode = ( (title === 'Add Contact') ? 'create' : 'update' );
                    onSubmit( values, bag, mode );
                } } >
                { ({ submitForm, isSubmitting, errors })=> (
                    <Container className={ classes.FormContainer } maxWidth='xs'>
                        <Card>
                            <CardContent>
                                <Box className={ classes.FormCardHeading }>
                                    <Typography 
                                        className={ classes.FormCardTitle }
                                        variant='h5'>
                                        { title }
                                    </Typography>
                                    <IconButton
                                        color='primary'
                                        aria-label='Cancel'
                                        onClick={ onCancel }>
                                        <Icon>clear</Icon>
                                    </IconButton>
                                </Box>
                                <Form className={ classes.Form } >
                                    <Field fullWidth
                                        component={ TextField }
                                        id='first_name'
                                        name='first_name'
                                        label='First Name'
                                        type='text'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='first_name' />

                                    <Field fullWidth
                                        component={ TextField }
                                        id='last_name'
                                        name='last_name'
                                        label='Last Name'
                                        type='text'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='last_name' />

                                    <Field fullWidth
                                        component={ TextField }
                                        id='work'
                                        name='work'
                                        label='Work'
                                        type='text'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='work' />

                                    <Field fullWidth
                                        component={ TextField }
                                        id='phone'
                                        name='phone'
                                        label='Phone'
                                        type='text'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='phone' />

                                    <Field fullWidth
                                        component={ TextField }
                                        id='email'
                                        name='email'
                                        label='Email'
                                        type='email'
                                        variant='outlined'
                                        margin='normal'
                                        autoComplete='email' />
                                        
                                    <Button fullWidth 
                                        className={ classes.Submit }
                                        type='submit'
                                        variant='contained'
                                        color='primary'
                                        disabled={ ( isSubmitting || ( ( Object.values( errors ) ).length > 0 ) ) }
                                        aria-label='Submit'
                                        onClick={ submitForm } >
                                        Save
                                    </Button>
                                    
                                </Form>
                            </CardContent>
                        </Card>
                    </Container>
                ) }
            </Formik>

		</Container>
	);
};