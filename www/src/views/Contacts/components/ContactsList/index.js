
import React from 'react';

import MaterialTable from 'material-table';

import { makeStyles } from '@material-ui/core/styles';

import Container from '@material-ui/core/Container';

const useStyles = makeStyles( (theme)=> ({
	ContactsListContainer: {
		flexGrow: 1,
		padding: theme.spacing( 1 )
	},
}) );

export const ContactsList = (props)=> {
	const {
        data,
        onCreate, onEdit, onDelete
    } = props;

	const classes = useStyles();

	return (
		<Container
			className={ classes.ContactsListContainer }
			maxWidth={ false } >

			<MaterialTable
				style={{ height: '100%' }}
				title='Contacts'
				columns={[
					{ title: 'First Name',		field: 'first_name' },
					{ title: 'Last Name',    	field: 'last_name' },
					{ title: 'Email',    		field: 'email' }
				]}
				options={{ paging: false }}
                data={ data }
                actions={[ {
					icon: 'add',
					tooltip: 'Add Contact',
					isFreeAction: true,
					onClick: (event)=> {
						onCreate( event );
					}
				}, {
					icon: 'edit',
					tooltip: 'Edit Contact',
					onClick: (event, rowData)=> {
						onEdit( event, rowData );
					}
				}, {
					icon: 'clear',
					tooltip: 'Delete Contact',
					onClick: (event, rowData)=> {
						onDelete( event, rowData );
					}
				} ]} />
		</Container>
	);
};