
import React from 'react';

import { render, fireEvent, cleanup, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';

import { ContactsList } from './index';

const renderWithRouter = (ui, { route='/', history=( createMemoryHistory({ initialEntries: [route] }) ) }={})=> {
    return {
        ...render( <Router history={ history }>{ ui }</Router> ),
        history
    };
};

const defaultProps = (props)=> ({
    contacts: [],
    ...props
});

afterEach( cleanup );

/* Testing any more than the default render would essentialy double-test the third-party library */
describe( 'render', ()=> {
    describe( 'default', ()=> {    
        it( 'should render the Contact List', ()=> {
            const props = defaultProps();
            const { getByText } = renderWithRouter( <ContactsList { ...props } /> );

            const contactDetailsForm = getByText( 'Contacts' );
            expect( contactDetailsForm ).toBeInTheDocument();
        } );
    } );
} );