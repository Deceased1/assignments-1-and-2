
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/styles';

import { NET } from '../../net';

import * as app from '../../App';

import { Base, styles } from './Contacts.jsx';

/* Types */
export const types = {
    CONTACTS_SET_CONTACTS: 'CONTACTS_SET_CONTACTS',
    CONTACTS_SET_FORM_TITLE: 'CONTACTS_SET_FORM_TITLE',
    CONTACTS_SET_FORM_IS_OPEN: 'CONTACTS_SET_FORM_IS_OPEN',
    CONTACTS_SET_FORM_FIELDS: 'CONTACTS_SET_FORM_FIELDS',
    CONTACTS_SET_DIALOG_IS_OPEN: 'CONTACTS_SET_DIALOG_IS_OPEN',
    CONTACTS_SET_DIALOG_CONTACT: 'CONTACTS_SET_DIALOG_CONTACT'
};

/* Actions */
export const actions = {
    contactsSetContacts: (contacts)=> ({ type: types.CONTACTS_SET_CONTACTS, contacts }),
    contactsSetFormTitle: (title)=> ({ type: types.CONTACTS_SET_FORM_TITLE, title }),
    contactsSetFormIsOpen: (isOpen)=> ({ type: types.CONTACTS_SET_FORM_IS_OPEN, isOpen }),
    contactsSetFormFields: (fields)=> ({ type: types.CONTACTS_SET_FORM_FIELDS, fields }),
    contactsSetDialogIsOpen: (isOpen)=> ({ type: types.CONTACTS_SET_DIALOG_IS_OPEN, isOpen }),
    contactsSetDialogContact: (contact)=> ({ type: types.CONTACTS_SET_DIALOG_CONTACT, contact })
};

/* Initial State */
export const initialState = {
	identifier: 'contacts',
	icon: 'contacts',
	name: 'Contacts',
    url: '/contacts',

    contacts: [],

    contactsForm: {
        title: '',
        isOpen: false,
        fields: {
            first_name: '',
            last_name: '',
            work: '',
            phone: '',
            email: ''
        },
        errors: []
    },

    contactsDialog: {
        isOpen: false,
        contact: {
            first_name: '',
            last_name: '',
        }
    }
};

/* Reducer */
export const contactsReducer = (state={...initialState}, action)=> {
    let stateReduction;
    switch ( action.type ) {
        case types.CONTACTS_SET_CONTACTS: {
            stateReduction = { ...state,
                contacts: action.contacts
            }; break;
        }
        case types.CONTACTS_SET_FORM_TITLE: {
            stateReduction = { ...state,
                contactsForm: { ...state.contactsForm,
                    title: action.title
                }
            }; break;
        }
        case types.CONTACTS_SET_FORM_IS_OPEN: {
            stateReduction = { ...state,
                contactsForm: { ...state.contactsForm,
                    isOpen: action.isOpen
                }
            }; break;
        }
        case types.CONTACTS_SET_FORM_FIELDS: {
            stateReduction = { ...state,
                contactsForm: { ...state.contactsForm,
                    fields: action.fields
                }
            }; break;
        }
        case types.CONTACTS_SET_DIALOG_IS_OPEN: {
            stateReduction = { ...state,
                contactsDialog: { ...state.contactsDialog,
                    isOpen: action.isOpen
                }
            }; break;
        }
        case types.CONTACTS_SET_DIALOG_CONTACT: {
            stateReduction = { ...state,
                contactsDialog: { ...state.contactsDialog,
                    contact: action.contact
                }
            }; break;
        }
        default: stateReduction = { ...state };
    }
    return {
        ...stateReduction
    }
};

const mapStateToProps = (state)=> ({
    ...state,
    ...state.modules.contacts,
});

const mapDispatchToProps = (dispatch)=> ({
	activateModule: (identifier)=> {
		dispatch( app.actions.activateModule( identifier ) );
	},
	deactivateModule: (identifier)=> {
		dispatch( app.actions.deactivateModule( identifier ) );
    },

    fetchContacts: async (token)=> {
        dispatch( app.actions.appHeaderSetIsLoading( true ) );

        let contacts = await NET.API_V1.CONTACTS.ALL( token );
        dispatch( actions.contactsSetContacts( contacts ) );
        
        dispatch( app.actions.appHeaderSetIsLoading( false ) );
    },

    onAddContact: async (details, token)=> {
        dispatch( app.actions.appHeaderSetIsLoading( true ) );
        
        await NET.API_V1.CONTACTS.ADD( details, token );
        
        // Refresh the list of contacts...
        let contacts = await NET.API_V1.CONTACTS.ALL( token );
        dispatch( actions.contactsSetContacts( contacts ) );
        
        dispatch( actions.contactsSetFormIsOpen( false ) );
        
        dispatch( app.actions.appHeaderSetIsLoading( false ) );
    },

    onEditContact: async (details, token)=> {
        dispatch( app.actions.appHeaderSetIsLoading( true ) );

        delete details.tableData;
        await NET.API_V1.CONTACTS.EDIT( details.id, details, token );
        
        // Refresh the list of contacts...
        let contacts = await NET.API_V1.CONTACTS.ALL( token );
        dispatch( actions.contactsSetContacts( contacts ) );
        
        dispatch( actions.contactsSetFormIsOpen( false ) );
        
        dispatch( app.actions.appHeaderSetIsLoading( false ) );
    },

    onRemoveContact: async (details, token)=> {
        dispatch( app.actions.appHeaderSetIsLoading( true ) );

        await NET.API_V1.CONTACTS.REMOVE( details.id, token );
        
        // Refresh the list of contacts...
        let contacts = await NET.API_V1.CONTACTS.ALL( token );
        dispatch( actions.contactsSetContacts( contacts ) );
        
        dispatch( actions.contactsSetDialogIsOpen( false ) );
        dispatch( actions.contactsSetDialogContact({ first_name: '', last_name: '' }) );
        
        dispatch( app.actions.appHeaderSetIsLoading( false ) );
    },

    openForm: (title, target)=> {
        dispatch( app.actions.appHeaderSetIsLoading( true ) );
        
        if ( target ) {
            dispatch( actions.contactsSetFormFields({ ...target }) );
        }

        dispatch( actions.contactsSetFormTitle( title ) );
        dispatch( actions.contactsSetFormIsOpen( true ) );
        
        dispatch( app.actions.appHeaderSetIsLoading( false ) );
    },

    openDialog: (target)=> {
        dispatch( app.actions.appHeaderSetIsLoading( true ) );
        
        dispatch( actions.contactsSetDialogContact( target ) );
        dispatch( actions.contactsSetDialogIsOpen( true ) );
        
        dispatch( app.actions.appHeaderSetIsLoading( false ) );
    },

    closeDialog: ()=> {
        dispatch( app.actions.appHeaderSetIsLoading( true ) );
        
        dispatch( actions.contactsSetDialogIsOpen( false ) );
        dispatch( actions.contactsSetDialogContact({ first_name: '', last_name: '' }) );
        
        dispatch( app.actions.appHeaderSetIsLoading( false ) );
    },
    
    resetForm: ()=> {
        dispatch( app.actions.appHeaderSetIsLoading( true ) );

        dispatch( actions.contactsSetFormFields({
            first_name: '',
            last_name: '',
            work: '',
            phone: '',
            email: ''
        }) );

        dispatch( actions.contactsSetFormIsOpen( false ) );
        
        dispatch( app.actions.appHeaderSetIsLoading( false ) );
    }
    
});

export { Base as ContactsComponent };
export const Contacts = ( connect( mapStateToProps, mapDispatchToProps )( withStyles( styles )( Base ) ) );