
import React from 'react';

import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import { makeStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Snackbar from '@material-ui/core/Snackbar';
import Typography from '@material-ui/core/Typography';

import { TextField } from 'formik-material-ui';

const useStyles = makeStyles( (theme)=> ({
    ProfileFormRoot: {
        alignItems: 'center',
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'center',
    },
    FormContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    FormCardHeading: {
        display: 'flex'
    },
    FormCardHeadingTitle: {
        flexGrow: 1
    },
    Form: {
        marginTop: theme.spacing( 1 ),
        width: '100%',
    },
    FormGrid: {
        marginBottom: theme.spacing( 1 )
    },
    ButtonReset: {
        marginRight: theme.spacing( 1 ),
    },
    ButtonSave: {
        marginLeft: theme.spacing( 1 ),
    }
}) );

const validationSchema = Yup.object().shape({
    username: Yup.string()
        .ensure()
        .required( 'Please specify a Username' ),
    first_name: Yup.string()
        .ensure()
        .required( 'Please specify your First Name' ),
    last_name: Yup.string()
        .ensure()
        .required( 'Please specify your Last Name' ),
    date_of_birth: Yup.string()
        .ensure()
        .required( 'Please specify your Date of Birth' ),
    country: Yup.string()
        .ensure(),
    gender: Yup.string()
        .ensure(),
    contact_numbers: Yup.string()
        .ensure(),
    interests: Yup.string()
        .ensure()
});

export const ProfileForm = (props)=> {
    const classes = useStyles();

    const {
        fields, errors,
        resetPassword, onSubmit
    } = props;
    
    return (
        <div className={ classes.ProfileFormRoot }>
            <Formik
                initialValues={ fields }
                validateOnMount={ false }
                validationSchema={ validationSchema }
                enableReinitialize={ true }
                onSubmit={ (values, bag )=> {
                    onSubmit( values, bag );
                    bag.setSubmitting( false );
                } } >
                { ({ submitForm, isSubmitting, errors })=> (
                    <Container className={ classes.FormContainer }>
                        <Card>
                            <CardContent>
                                <Box className={ classes.FormCardHeading }>
                                    <Typography gutterBottom
                                        className={ classes.FormCardHeadingTitle }
                                        variant="h4">
                                        Profile
                                    </Typography>
                                    <Button 
                                        variant='contained'
                                        color='primary'
                                        disabled={ true }
                                        onClick={ resetPassword } >
                                        Reset Password
                                    </Button>
                                </Box>
                                <Form className={ classes.Form } >
                                    
                                    <Grid container
                                        className={ classes.FormGrid }
                                        spacing={ 3 }>
                                        <Grid item sm={ 6 }>

                                            <Field fullWidth
                                                component={ TextField }
                                                id='username'
                                                name='username'
                                                label='Username'
                                                type='text'
                                                variant='outlined'
                                                margin='normal'
                                                autoComplete='username' />

                                            <Field fullWidth
                                                component={ TextField }
                                                id='first_name'
                                                name='first_name'
                                                label='First Name'
                                                type='text'
                                                variant='outlined'
                                                margin='normal'
                                                autoComplete='first_name' />
                                                
                                            <Field fullWidth
                                                component={ TextField }
                                                name='last_name'
                                                label='Last Name'
                                                type='text'
                                                variant='outlined'
                                                margin='normal'
                                                autoComplete='last_name' />

                                            <Field fullWidth
                                                component={ TextField }
                                                id='date_of_birth'
                                                name='date_of_birth'
                                                label='Date of Birth'
                                                type='text'
                                                variant='outlined'
                                                margin='normal'
                                                autoComplete='date_of_birth' />
                                        </Grid>
                                        <Grid item sm={ 6 }>
                                            <Field fullWidth
                                                component={ TextField }
                                                id='country'
                                                name='country'
                                                label='Country'
                                                type='text'
                                                variant='outlined'
                                                margin='normal'
                                                autoComplete='country' />

                                            <Field fullWidth
                                                component={ TextField }
                                                id='gender'
                                                name='gender'
                                                label='Gender'
                                                type='text'
                                                variant='outlined'
                                                margin='normal'
                                                autoComplete='gender' />

                                            <Field fullWidth
                                                component={ TextField }
                                                id='contact_numbers'
                                                name='contact_numbers'
                                                label='Contact Numbers'
                                                type='text'
                                                variant='outlined'
                                                margin='normal'
                                                autoComplete='contact_numbers' />

                                            <Field fullWidth
                                                component={ TextField }
                                                id='interests'
                                                name='interests'
                                                label='Interests'
                                                type='text'
                                                variant='outlined'
                                                margin='normal'
                                                autoComplete='interests' />
                                        </Grid>
                                    </Grid>

                                    <Box display="flex">
                                        <Button fullWidth 
                                            className={ classes.ButtonSave }
                                            type='submit'
                                            variant='contained'
                                            color='primary'
                                            disabled={ ( isSubmitting || ( ( Object.values( errors ) ).length > 0 ) ) }
                                            onClick={ submitForm } >
                                            Save
                                        </Button>
                                    </Box>
                                    
                                </Form>
                            </CardContent>
                        </Card>
                    </Container>
                ) }
            </Formik>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={ ( (errors[ 0 ] !== undefined) ? true : false ) }
                autoHideDuration={ 6000 }
                message={
                    <Typography variant='body1' color='error'>
                        { ( (errors[ 0 ]) ? errors[ 0 ] : null ) }
                    </Typography>
                } />
        </div>
    );
};