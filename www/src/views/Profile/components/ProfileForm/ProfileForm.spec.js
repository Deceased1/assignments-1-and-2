
import React from 'react';

import { render, fireEvent, cleanup, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';

import { ProfileForm } from './index';

const renderWithRouter = (ui, { route='/', history=( createMemoryHistory({ initialEntries: [route] }) ) }={})=> {
    return {
        ...render( <Router history={ history }>{ ui }</Router> ),
        history
    };
};

const defaultProps = (props)=> ({
    fields: {
        email: '',
        username: '',
        first_name: '',
        last_name: '',
        date_of_birth: '',
        country: '',
        gender: '',
        contact_numbers: '',
        interests: ''
    },
    errors: [],
    resetForm: jest.fn(),
    onSubmit: jest.fn(),
    ...props
});

afterEach( cleanup );

describe( 'render', ()=> {
    describe( 'default', ()=> {    
        it( 'should render the Profile Form', ()=> {
            const props = defaultProps();
            const { getByText } = renderWithRouter( <ProfileForm { ...props } /> );

            const loginForm = getByText( 'Profile' );
            expect( loginForm ).toBeInTheDocument();
        } );
        it( 'should enable the Submit button by default', ()=> {
            const props = defaultProps();
            const { getByText } = renderWithRouter( <ProfileForm { ...props } /> );

            const submitButon = getByText( 'Save' );
            expect( submitButon ).not.toHaveAttribute( 'disabled' );
        } );
    } );
} );

describe( 'interaction', ()=> {
    describe( 'touching fields', ()=> {
        it( 'should produce errors for empty fields', async ()=> {
            const props = defaultProps();
            const { getByLabelText, findByText } = renderWithRouter( <ProfileForm { ...props } /> );
    
            const usernameField = getByLabelText( 'Username' );
            
            fireEvent.blur( usernameField );
        
            expect( await findByText( /Please specify a Username/i ) ).not.toBeNull();
        } );
    } );
    describe( 'submitting', ()=> {
        it( 'should call the appropriate function to submit the Profile-Update-Request', async ()=> {
            const props = defaultProps({ fields: {
                email: 'user@test.com',
                username: 'test-user',
                first_name: 'test',
                last_name: 'user',
                date_of_birth: '01-01-1970T00:00:00',
                country: 'test-country',
                gender: 'test-gender',
                contact_numbers: '0000000000',
                interests: 'test-interest'
            } });
            const { getByText } = renderWithRouter( <ProfileForm { ...props } /> );
    
            const submitButon = getByText( 'Save' );
            fireEvent.click( submitButon );
            
            /* Use the 'wait' api for Formik submits */
            await wait( ()=> {
                expect( props.onSubmit ).toHaveBeenCalledTimes( 1 );
            } );
        } );
    } );
} );