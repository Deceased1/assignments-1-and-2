
import React from 'react';

import { Redirect } from 'react-router-dom';

import Container from '@material-ui/core/Container';

import { ProfileForm } from './components/ProfileForm';

export const styles = ()=> ({
    ProfileRoot: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        paddingLeft: '0',
        paddingRight: '0'
    }
});

/*
 * This component hosts the interface for managing the logged in users' details.
 */
export class Base extends React.PureComponent {

    componentDidMount() {
        const { user, token, fetchProfile } = this.props;
       
        fetchProfile( user.id, token );
    }
    
    render() {
        const {
            classes, user, token,
            profileForm,
            resetPassword, saveProfile
        } = this.props;

        if ( !user ) {
            return ( <Redirect to={ '/login' } /> );
        }

        return (
            <Container
                className={ classes.ProfileRoot }
                maxWidth={ false } >
                    
                <ProfileForm { ...profileForm }
                    resetPassword={ resetPassword }
                    onSubmit={ (values)=> { saveProfile( values, token ); } } />
            </Container>
        );
    }
};