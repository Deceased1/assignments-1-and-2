
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/styles';

import { NET } from '../../net';

import * as app from '../../App';

import { Base, styles } from './Profile.jsx';

/* Types */
export const types = {
    PROFILE_SET_FORM_FIELDS: 'PROFILE_SET_FORM_FIELDS',
};

/* Actions */
export const actions = {
    profileSetFormFields: (fields)=> ({ type: types.PROFILE_SET_FORM_FIELDS, fields })
};

/* Initial State */
export const initialState = {
    profile: undefined,
    profileForm: {
        fields: {
            email: '',
            username: '',
            first_name: '',
            last_name: '',
            date_of_birth: '',
            country: '',
            gender: '',
            contact_numbers: '',
            interests: ''
        },
        errors: []
    }
};

/* Reducer */
export const profileReducer = (state={...initialState}, action)=> {
    let stateReduction;
    switch ( action.type ) {
        case types.PROFILE_SET_USER_PROFILE: {
            stateReduction = { ...state,
                profile: action.profile
            };
        }; break;
        case types.PROFILE_SET_FORM_FIELDS: {
            stateReduction = { ...state,
                profileForm: { ...state.profileForm,
                    fields: action.fields
                }
            };
        }; break;
        default: stateReduction = { ...state };
    }
    return {
        ...stateReduction
    }
};

const mapStateToProps = (state)=> ({
    ...state,
    ...state.authentication.profile,
});

const mapDispatchToProps = (dispatch)=> ({
    fetchProfile: async (userId, token)=> {
        dispatch( app.actions.appHeaderSetIsLoading( true ) );
        
        let profile = await NET.API_V1.GET_PROFILE( userId, token );
        dispatch( actions.profileSetFormFields({ ...profile }) );
        
        dispatch( app.actions.appHeaderSetIsLoading( false ) );
    },

    resetPassword: async ()=> {
        // TODO: if there's time - implement this...
        alert( 'This needs to happen from the backend...' );
    },
    saveProfile: async (details, token)=> {
        dispatch( app.actions.appHeaderSetIsLoading( true ) );
        
        await NET.API_V1.UPDATE_PROFILE( details.id, details, token );
        
        dispatch( app.actions.appHeaderSetIsLoading( false ) );
    }
});

export { Base as ProfileComponent };
export const Profile = ( connect( mapStateToProps, mapDispatchToProps )( withStyles( styles )( Base ) ) );