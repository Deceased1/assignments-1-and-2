
import request from 'request';

const REGISTRY = {
    API_V1: {
        /*address: '127.0.0.1',*/
        address: '3.134.118.21',
        port: 2001,
        protocol: 'http',
        route: '/api/v1'
    }
};

export const NET = {
    API_V1: {
        LOGIN: (details)=> {
            return new Promise( (resolve)=> {
                let resource = `${REGISTRY.API_V1.protocol}://${REGISTRY.API_V1.address}:${REGISTRY.API_V1.port}${REGISTRY.API_V1.route}/login`
                request.post( resource, { json: details }, (err, httpResponse, responseBody)=> {
                    resolve( responseBody );
                } );
            } );
        },
        REGISTER: (details)=> {
            return new Promise( (resolve)=> {
                let resource = `${REGISTRY.API_V1.protocol}://${REGISTRY.API_V1.address}:${REGISTRY.API_V1.port}${REGISTRY.API_V1.route}/register`
                request.post( resource, { json: details }, (err, httpResponse, responseBody)=> {
                    resolve( responseBody );
                } );
            } );
        },
        LOGOUT: (token)=> {
            return new Promise( (resolve)=> {
                let resource = `${REGISTRY.API_V1.protocol}://${REGISTRY.API_V1.address}:${REGISTRY.API_V1.port}${REGISTRY.API_V1.route}/logout`
                request.delete( resource, { headers: { 'Authorization': `Bearer ${ token }` } }, (err, httpResponse, responseBody)=> {
                    resolve( JSON.parse( responseBody ) );
                } );
            } );
        },
        GET_PROFILE: (userId, token)=> {
            return new Promise( (resolve)=> {
                let resource = `${REGISTRY.API_V1.protocol}://${REGISTRY.API_V1.address}:${REGISTRY.API_V1.port}${REGISTRY.API_V1.route}/profiles/${ userId }`
                request.get( resource, { headers: { 'Authorization': `Bearer ${ token }` } }, (err, httpResponse, responseBody)=> {
                    resolve( JSON.parse( responseBody ) );
                } );
            } );
        },
        UPDATE_PROFILE: (profileId, updates, token)=> {
            return new Promise( (resolve)=> {
                let resource = `${REGISTRY.API_V1.protocol}://${REGISTRY.API_V1.address}:${REGISTRY.API_V1.port}${REGISTRY.API_V1.route}/profiles/${ profileId }`
                request.put( resource, { headers: { 'Authorization': `Bearer ${ token }` }, json: updates }, (err, httpResponse, responseBody)=> {
                    resolve( responseBody );
                } );
            } );
        },
        CONTACTS: {
            ALL: (token)=> {
                return new Promise( (resolve)=> {
                    let resource = `${REGISTRY.API_V1.protocol}://${REGISTRY.API_V1.address}:${REGISTRY.API_V1.port}${REGISTRY.API_V1.route}/contacts`
                    request.get( resource, { headers: { 'Authorization': `Bearer ${ token }` } }, ( err, httpResponse, responseBody )=> {
                        resolve( JSON.parse( responseBody ) );
                    } );
                } );
            },
            BY_ID: (contactId, token)=> {
                return new Promise( (resolve)=> {
                    let resource = `${REGISTRY.API_V1.protocol}://${REGISTRY.API_V1.address}:${REGISTRY.API_V1.port}${REGISTRY.API_V1.route}/contacts/${contactId}`
                    request.get( resource, { headers: { 'Authorization': `Bearer ${ token }` } }, (err, httpResponse, responseBody)=> {
                        resolve( JSON.parse( responseBody ) );
                    } );
                } );
            },
            ADD: (contact, token)=> {
                return new Promise( (resolve)=> {
                    let resource = `${REGISTRY.API_V1.protocol}://${REGISTRY.API_V1.address}:${REGISTRY.API_V1.port}${REGISTRY.API_V1.route}/contacts`
                    request.post( resource, { headers: { 'Authorization': `Bearer ${ token }` }, json: contact }, (err, httpResponse, responseBody)=> {
                        resolve( responseBody );
                    } );
                } );
            },
            EDIT: (contactId, updates, token)=> {
                return new Promise( (resolve)=> {
                    let resource = `${REGISTRY.API_V1.protocol}://${REGISTRY.API_V1.address}:${REGISTRY.API_V1.port}${REGISTRY.API_V1.route}/contacts/${contactId}`
                    request.put( resource, { headers: { 'Authorization': `Bearer ${ token }` }, json: updates }, (err, httpResponse, responseBody)=> {
                        resolve( responseBody );
                    } );
                } );
            },
            REMOVE: (contactId, token)=> {
                return new Promise( (resolve)=> {
                    let resource = `${REGISTRY.API_V1.protocol}://${REGISTRY.API_V1.address}:${REGISTRY.API_V1.port}${REGISTRY.API_V1.route}/contacts/${contactId}`
                    request.delete( resource, { headers: { 'Authorization': `Bearer ${ token }` } }, (err, httpResponse, responseBody)=> {
                        resolve( JSON.parse( responseBody ) );
                    } );
                } );
            }
        }
    }
};