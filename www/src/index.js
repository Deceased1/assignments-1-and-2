
import './index.css';

import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore } from 'redux';

import { BrowserRouter as Router } from 'react-router-dom';

import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

import { App, appReducer } from './App';

export const store = createStore( appReducer );

const palette = {
    primary: { main: '#263238' },
    secondary: { main: '#E53935' }
};
const themeName = 'Outer Space Cinnabar Kangaroo Rat';
const defaultTheme = createMuiTheme({ palette, themeName });

ReactDOM.render(
    <ThemeProvider theme={ defaultTheme }>
        <Provider store={ store } >
            <Router >
                <App />
            </Router>
        </Provider>
    </ThemeProvider>,
    document.getElementById( 'root' )
);