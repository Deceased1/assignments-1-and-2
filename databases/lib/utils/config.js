
const path = require( 'path' );

const dotenv = require( 'dotenv' );
const convict = require( 'convict' );

dotenv.config();

const config = convict({
    env: {
        doc: "The environment configuration",
        format: [ "production", "staging", "testing", "development" ],
        default: "development",
        env: "NODE_ENV"
    },
    service: {
        name: {
            doc: "The service name",
            format: String,
            default: "My Service",
            env: "SERVICE_NAME"
        },
        internalIP: {
            doc: 'The private IP address',
            format: 'ipaddress',
            default: '127.0.0.1',
        },
        externalIP: {
            doc: 'The public IP address',
            format: 'ipaddress',
            default: '127.0.0.1',
        },
        port: {
            doc: 'The port to bind.',
            format: 'port',
            default: '8080',
            env: 'PORT'
        }
    },
    db: {
        host: {
            doc: 'Database host name/IP',
            format: '*',
            default: '127.0.0.1',
            env: 'DATABASE_HOST'
        },
        port: {
            doc: 'Database port',
            format: 'port',
            default: '27017',
            env: 'DATABASE_PORT'
        },
        database: {
            doc: 'Database name',
            format: String,
            default: 'local',
            env: 'DATABASE_NAME'
        },
        username: {
            doc: 'Database username',
            format: String,
            default: 'username',
            env: 'DATABASE_USERNAME'
        },
        password: {
            doc: 'Database password',
            format: String,
            default: 'password',
            env: 'DATABASE_PASSWORD'
        },
        schema: {
            doc: 'Database schema if applicable',
            format: String,
            default: 'public'
        },
        migrations: {
            directory: {
                doc: 'Database migrations directory',
                format: String,
                default: `${ __dirname }/db/migrations`,
            }
        },
        seeds: {
            directory: {
                doc: 'Database seeds directory',
                format: String,
                default: `${ __dirname }/db/seeds/`
            }
        }
    }
});

const env = config.get( 'env' );
config.loadFile( path.resolve( `./configurations/${ env }.json` ) );

config.validate({ allowed: 'strict' });

module.exports = config.getProperties();