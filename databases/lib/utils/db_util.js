
const knex = require( '../../knex' );

module.exports = {
    setup: (done)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                await knex.migrate.rollback();
                await knex.migrate.latest();
                await knex.seed.run();
                resolve( done() );
            } catch( err ) {
                reject( err );
            }
        } );
    },
    purge: ()=> {
        return new Promise( async (resolve, reject)=> {
            try {
                await knex.migrate.rollback();
                resolve();
            } catch( err ) {
                reject( err );
            }
        } );
    },
    migrate: ()=> {
        return new Promise( async (resolve, reject)=> {
            try {
                await knex.migrate.latest();
                resolve();
            } catch( err ) {
                reject( err );
            }
        } );
    },
    seed: ()=> {
        return new Promise( async (resolve, reject)=> {
            try {
                await knex.seed.run();
                resolve();
            } catch( err ) {
                reject( err );
            }
        } );
    }
};