
exports.up = function( knex ) {
	return knex.schema.createTable( '2es_user', (table)=> {
		table.uuid( 'id' ).notNullable().primary();
		table.string( 'date_created' ).notNullable().defaultTo( new Date().toISOString() );
		table.string( 'date_modified' ).notNullable().defaultTo( new Date().toISOString() );
		table.boolean( 'deleted' ).notNullable().defaultTo( false );
        table.string( 'email' ).notNullable().defaultTo( '' );
		table.string( 'hash' ).notNullable();
		table.string( 'salt' ).notNullable();
	} );
};

exports.down = function( knex ) {
	return knex.schema.dropTable( '2es_user' );
};
