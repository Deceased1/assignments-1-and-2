
exports.up = function( knex ) {
	return knex.schema.createTable( '2es_session', ( table )=> {
		table.uuid( 'id' ).notNullable().primary();
		table.string( 'date_created' ).notNullable().defaultTo( new Date().toISOString() );
		table.string( 'date_modified' ).notNullable().defaultTo( new Date().toISOString() );
		table.boolean( 'deleted' ).notNullable().defaultTo( false );
		table.uuid( 'token_id' ).notNullable();
        table.boolean( 'signed_out' ).notNullable().defaultTo( false );
		table.uuid( 'user_fk' ).references( '2es_user.id' ).onDelete( 'CASCADE' );
	} );
};

exports.down = function( knex ) {
	return knex.schema.dropTable( '2es_session' );
};
