
exports.up = function( knex ) {
	return knex.schema.createTable( '2es_contact', (table)=> {
		table.uuid( 'id' ).notNullable().primary();
		table.string( 'date_created' ).notNullable().defaultTo( new Date().toISOString() );
		table.string( 'date_modified' ).notNullable().defaultTo( new Date().toISOString() );
		table.boolean( 'deleted' ).notNullable().defaultTo( false );
		table.string( 'first_name' ).notNullable().defaultTo( '' );
        table.string( 'last_name' ).notNullable().defaultTo( '' );
        table.string( 'work' ).notNullable().defaultTo( '' );
        table.string( 'phone' ).notNullable().defaultTo( '' );
        table.string( 'email' ).notNullable().defaultTo( '' );
		table.uuid( 'user_fk' ).references( '2es_user.id' ).onDelete( 'CASCADE' );
	} );
};

exports.down = function( knex ) {
	return knex.schema.dropTable( '2es_contact' );
};
