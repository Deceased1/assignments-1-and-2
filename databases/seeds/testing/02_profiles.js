
const profiles = [
	{ id: '00000000-0000-0000-0000-000000000001', username: 'testUserOne', first_name: 'testUser', last_name: 'One', date_of_birth: '1970-01-01', country: 'USA', gender: 'M', contact_numbers: '', interests: '', user_fk: '00000000-0000-0000-0000-000000000001' },
	{ id: '00000000-0000-0000-0000-000000000002', username: 'testUserTwo', first_name: 'testUser', last_name: 'Two', date_of_birth: '1970-01-01', country: 'RSA', gender: 'M', contact_numbers: '', interests: '', user_fk: '00000000-0000-0000-0000-000000000002' },
	{ id: '00000000-0000-0000-0000-000000000003', username: 'testUserThree', first_name: 'testUser', last_name: 'Three', date_of_birth: '1970-01-01', country: 'JPN', gender: 'F', contact_numbers: '', interests: '', user_fk: '00000000-0000-0000-0000-000000000003' },
];

exports.profiles = profiles;

exports.seed = function( knex ) {
	return knex( '2es_profile' ).del()
		.then( ()=> {
			return knex( '2es_profile' ).insert( profiles );
		});
};