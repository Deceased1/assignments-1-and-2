
const contacts = [
	{ id: '00000000-0000-0000-0000-000000000001', first_name: 'testContact', last_name: 'One', work: '', phone: '', email: '', user_fk: '00000000-0000-0000-0000-000000000001' },
	{ id: '00000000-0000-0000-0000-000000000002', first_name: 'testContact', last_name: 'Two', work: '', phone: '', email: '', user_fk: '00000000-0000-0000-0000-000000000001' },
	{ id: '00000000-0000-0000-0000-000000000003', first_name: 'testContact', last_name: 'One', work: '', phone: '', email: '', user_fk: '00000000-0000-0000-0000-000000000002' },
	{ id: '00000000-0000-0000-0000-000000000004', first_name: 'testContact', last_name: 'Three', work: '', phone: '', email: '', user_fk: '00000000-0000-0000-0000-000000000001' },
	{ id: '00000000-0000-0000-0000-000000000005', first_name: 'testContact', last_name: 'One', work: '', phone: '', email: '', user_fk: '00000000-0000-0000-0000-000000000003' },
	{ id: '00000000-0000-0000-0000-000000000006', first_name: 'testContact', last_name: 'Two', work: '', phone: '', email: '', user_fk: '00000000-0000-0000-0000-000000000003' },
];

exports.contacts = contacts;

exports.seed = function( knex ) {
	return knex( '2es_contact' ).del()
		.then( ()=> {
			return knex( '2es_contact' ).insert( contacts );
		});
};