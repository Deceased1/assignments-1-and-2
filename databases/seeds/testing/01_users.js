
const users = [
	{ id: '00000000-0000-0000-0000-000000000001', email: 'tu1@test.com', hash: '$2a$04$r3rDryPadurKQCh2twgHXeVQjXHavfS8SycS3XSBmr413q9abF8Ri', salt: '$2a$12$cG9JZT8UrEmo0dThzh.OJO' },
	{ id: '00000000-0000-0000-0000-000000000002', email: 'tu2@test.com', hash: '$2a$04$r3rDryPadurKQCh2twgHXeVQjXHavfS8SycS3XSBmr413q9abF8Ri', salt: '$2a$12$cG9JZT8UrEmo0dThzh.OJO' },
	{ id: '00000000-0000-0000-0000-000000000003', email: 'tu3@test.com', hash: '$2a$04$r3rDryPadurKQCh2twgHXeVQjXHavfS8SycS3XSBmr413q9abF8Ri', salt: '$2a$12$cG9JZT8UrEmo0dThzh.OJO' }
];

exports.users = users;

exports.seed = function( knex ) {
	return knex( '2es_user' ).del()
		.then( ()=> {
			return knex( '2es_user' ).insert( users );
		});
};