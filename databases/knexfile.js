
const path = require( 'path' );

const config = require( './lib/utils/config' );

const getConnection = ()=> {
    return {
        user: config.db.username,
        password: config.db.password,
        database: config.db.database
    };
};

const getMigrations = ()=> {
    return {
        directory: config.db.migrations.directory
    };
};

const getSeeds = ()=> {
    return {
        directory: ( path.resolve( __dirname, config.db.seeds.directory ) )
    };
};

module.exports = {
	testing: {
		client: 'mysql',
		connection: getConnection(),
		migrations: getMigrations(),
		seeds: getSeeds()
	},
	development: {
		client: 'mysql',
		connection: getConnection(),
		migrations: getMigrations(),
		seeds: getSeeds()
	},
	staging: {
		client: 'mysql',
		connection: getConnection(),
		migrations: getMigrations(),
		seeds: getSeeds()
	},
	production: {
		client: 'mysql',
		connection: getConnection(),
		migrations: getMigrations(),
		seeds: getSeeds()
	}
};