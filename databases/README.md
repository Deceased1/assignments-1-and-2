
# Database / Devices

This is a MySQL database.

There is a different schema defined for each environment as follows:
* Development:  2e_systems_dev
* Staging:      2e_systems_stag
* Production:   2e_systems_prod
* Testing:      2e_systems_test

For fluid database management, ***Knex*** is used as a first layer of abstraction. This allows seamless migration and seeding functionality of tables to be created. A drawback, unfortunately, is that the databases will have to be created manually ( unless scripts for this is introduced ).

This can be done with:
```
CREATE DATABASE IF NOT EXISTS <db_name>;
```

## Configuration

This project runs off a .env ( dotenv ) configuration and Convict.

To create a dotenv file, in the project root:
```
touch .env
```

This dotenv file needs at least the following values filled out:
* NODE_ENV={ testing | production | development | staging }
* DATABASE_USERNAME={ *db_user_name* }
* DATABASE_PASSWORD={ *db_user_password* }
* DATABASE_HOST={ *db_address* }
* DATABASE_PORT={ *db_port* }

### Migrations

As mentioned, ***Knex*** is used to manage the database, as such, the *migration* functionality is used to setup and teardown the database tables.
To run a ***Knex*** migration-script, either install the ***Knex CLI-Tool*** and run the migration-command from there or issue the following command from the project-root directory:
```
node_modules/.bin/knex migrate:latest
```

**Note**: this will run the migration to the Node Environment specified in the .env file.

### Seeds

As mentioned, ***Knex*** is used to manage the database, as such, the *seed* functionality is used to populate the tables.
To run a ***Knex*** seed-script, either install the ***Knex CLI-Tool*** and run the seed-command from there or issue the following command from the project-root directory:
```
node_modules/.bin/knex seed:run
```

**Note**: this will run the seed to the Node Environment specified in the .env file.