
const DBUtil = require( './lib/utils/db_util' );

const argv = require( 'yargs' )
    .usage( 'Usage: $0 [options]' )
    .example( '$0 -p -m -s', 'purge, migrate and seed the database schema' )
    .option( 'purge', {
        alias: 'p',
        description: 'Purge the DB Schema'
    } )
    .option( 'migrate', {
        alias: 'm',
        description: 'Migrate the DB Schema'
    } )
    .option( 'seed', {
        alias: 's',
        description: 'Seed the DB Schema'
    } )
    .help( 'help' )
    .alias( 'help', 'h' )
    .argv;

const runner = async ()=> {
    try {
        if ( argv.purge ) {
            await DBUtil.purge();
        }
        if ( argv.migrate ) {
            await DBUtil.migrate();
        }
        if ( argv.seed ) {
            await DBUtil.seed();
        }
    } catch( err ) {
        console.log( err );
    } finally {
        process.exit();
    }
};

runner();