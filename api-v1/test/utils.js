
const { spawn } = require( 'child_process' );
const path = require( 'path' );

const config = require( '../lib/utils/config' );

module.exports = {
    DB: {
        setup: ()=> {
            return new Promise( (resolve, reject)=> {
                let setup = spawn( `${ config.knex_wrapper }/bin/setup.sh`, [], { cwd: config.knex_wrapper, detached: true } );
                setup.on( 'error', (err)=> { reject( err ); } );
                setup.on( 'close', (code)=> { resolve( code ); } );
            } );
        },
        purge: ()=> {
            return new Promise( (resolve, reject)=> {
                let purge = spawn( `${ config.knex_wrapper }/bin/purge.sh`, [], { cwd: config.knex_wrapper, detached: true } );
                purge.on( 'error', (err)=> { reject( err ); } );
                purge.on( 'close', (code)=> { resolve( code ); } );
            } );
        },
        migrate: ()=> {
            return new Promise( (resolve, reject)=> {
                let migrate = spawn( `${ config.knex_wrapper }/bin/migrate.sh`, [], { cwd: config.knex_wrapper, detached: true } );
                migrate.on( 'error', (err)=> { reject( err ); } );
                migrate.on( 'close', (code)=> { resolve( code ); } );
            } );
        },
        seed: ()=> {
            return new Promise( (resolve, reject)=> {
                let seed = spawn( `${ config.knex_wrapper }/bin/seed.sh`, [], { cwd: config.knex_wrapper, detached: true } );
                seed.on( 'error', (err)=> { reject( err ); } );
                seed.on( 'close', (code)=> { resolve( code ); } );
            } );
        }
    },
    RANDOM: (min=0, max=1)=> {
        return Math.floor( Math.random() * ( max - min + 1 ) + min );
    },
    GET: (chai, options)=> {
        return new Promise( (resolve, reject)=> {
            try {
                chai.request( options.server )
                    .get( options.endpoint )
                    .end( (err, res)=> { resolve({ err, res }); } );
            } catch(err) {
                reject(err);
            }
        } );
    },
    GET_AUTH: (chai, options)=> {
        return new Promise( (resolve, reject)=> {
            try {
                chai.request( options.server )
                    .get( options.endpoint )
                    .set( 'Authorization', `Bearer ${ options.token }` )
                    .end( (err, res)=> { resolve({ err, res }); } );
            } catch(err) {
                reject(err);
            }
        } );
    },
    POST: (chai, options)=> {
        return new Promise( (resolve, reject)=> {
            try {
                chai.request( options.server )
                    .post( options.endpoint )
                    .send( options.data )
                    .end( (err, res)=> { resolve({ err, res }); } );
            } catch(err) {
                reject(err);
            }
        } );
    },
    POST_AUTH: (chai, options)=> {
        return new Promise( (resolve, reject)=> {
            try {
                chai.request( options.server )
                    .post( options.endpoint )
                    .set( 'Authorization', `Bearer ${ options.token }` )
                    .send( options.data )
                    .end( (err, res)=> { resolve({ err, res }); } );
            } catch(err) {
                reject(err);
            }
        } );
    },
    PUT: (chai, options)=> {
        return new Promise( (resolve, reject)=> {
            try {
                chai.request( options.server )
                    .put( options.endpoint )
                    .send( options.data )
                    .end( (err, res)=> { resolve({ err, res }); } );
            } catch(err) {
                reject( err );
            }
        } );
    },
    PUT_AUTH: (chai, options)=> {
        return new Promise( (resolve, reject)=> {
            try {
                chai.request( options.server )
                    .put( options.endpoint )
                    .set( 'Authorization', `Bearer ${ options.token }` )
                    .send( options.data )
                    .end( (err, res)=> { resolve({ err, res }); } );
            } catch(err) {
                reject( err );
            }
        } );
    },
    DELETE: (chai, options)=> {
        return new Promise( (resolve, reject)=> {
            try {
                chai.request( options.server )
                    .delete( options.endpoint )
                    .end( (err, res)=> { resolve({ err, res }); } );
            } catch(err) {
                reject( err );
            }
        } );
    },
    DELETE_AUTH: (chai, options)=> {
        return new Promise( (resolve, reject)=> {
            try {
                chai.request( options.server )
                    .delete( options.endpoint )
                    .set( 'Authorization', `Bearer ${ options.token }` )
                    .end( (err, res)=> { resolve({ err, res }); } );
            } catch(err) {
                reject( err );
            }
        } );
    },
};