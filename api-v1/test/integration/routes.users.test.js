
/* Always run tests on 'testing' environment */
process.env.NODE_ENV = 'testing';

const chai = require( 'chai' );
const chaiHTTP = require( 'chai-http' );
const should = chai.should();

chai.use( chaiHTTP );

const server = require( '../../lib/utils/server' );
const test_utils = require( '../utils' );

describe( 'routes : users', ()=> {

    let countResponse = undefined;
    let getAllResponse = undefined;
    let getByIdResponse = undefined;
    let addResponse = undefined;
    let editResponse = undefined;
    let removeResponse = undefined;
    
    before( async ()=> {
        await test_utils.DB.setup();

        loginResponse = await test_utils.POST( chai, {
            server: server.app,
            endpoint: '/api/v1/login',
            data: { email: 'tu1@test.com', password: 'asd' }
        } );

        countResponse = await test_utils.GET_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/count/users'
        } );
        addResponse = await test_utils.POST_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/users',
            data: {
                email: 'tuser@test.com',
                hash: '$2a$04$r3rDryPadurKQCh2twgHXeVQjXHavfS8SycS3XSBmr413q9abF8Ri',
                salt: '$2a$12$cG9JZT8UrEmo0dThzh.OJO'
            },
            token: loginResponse.res.body.token
        } );
        getAllResponse = await test_utils.GET_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/users',
            token: loginResponse.res.body.token
        });
        getByIdResponse = await test_utils.GET_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/users/00000000-0000-0000-0000-000000000001',
            token: loginResponse.res.body.token
        });
        editResponse = await test_utils.PUT_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/users/00000000-0000-0000-0000-000000000001',
            data: { email: 'tuser@test.com' },
            token: loginResponse.res.body.token
        } );
        removeResponse = await test_utils.DELETE_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/users/00000000-0000-0000-0000-000000000001',
            token: loginResponse.res.body.token
        } );
    } );

    // TODO: Not sure why this is failing...
    describe.skip( 'GET /api/v1/count/users', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( countResponse.err );
            countResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            countResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            countResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with a single users entry', (done)=> {
            should.not.exist( countResponse.res.body.length );
            should.exist( countResponse.res.body[ 'count(*)' ] );
            done();
        } );
    } );

    describe( 'POST /api/v1/users', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( addResponse.err );
            addResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            addResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            addResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with a single users entry', (done)=> {
            should.not.exist( addResponse.res.body.length );
            should.exist( addResponse.res.body.id );
            done();
        } );
        it( 'should have the correct data-structure', (done)=> {
            addResponse.res.body.should.include.keys(
                'id', 'date_created', 'date_modified', 'deleted', 'email'
            );
            done();
        } );
    } );

    describe( 'GET /api/v1/users', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( getAllResponse.err );
            getAllResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            getAllResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            getAllResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with all the users', (done)=> {
            getAllResponse.res.body.length.should.eql( 4 );
            done();
        } );
        it( 'should have the correct data-structure', (done)=> {
            getAllResponse.res.body[ 0 ].should.include.keys(
                'id', 'date_created', 'date_modified', 'deleted', 'email'
            );
            done();
        } );
    } );

    describe( 'GET /api/v1/users/:id', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( getByIdResponse.err );
            getByIdResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            getByIdResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            getByIdResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with a single users entry', (done)=> {
            should.not.exist( getByIdResponse.res.body.length );
            should.exist( getByIdResponse.res.body.id );
            done();
        } );
        it( 'should have the correct data-structure', (done)=> {
            getByIdResponse.res.body.should.include.keys(
                'id', 'date_created', 'date_modified', 'deleted', 'email'
            );
            done();
        } );
    } );

    describe( 'PUT /api/v1/users/:id', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( editResponse.err );
            editResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            editResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            editResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with a single users entry', (done)=> {
            should.not.exist( editResponse.res.body.length );
            should.exist( editResponse.res.body.id );
            done();
        } );
        it( 'should reflect the changes made', (done)=> {
            editResponse.res.body.email.should.equal( 'tuser@test.com' );
            done();
        } );
    } );

    // TODO: Not sure why this is failing...
    describe.skip( 'DELETE /api/v1/users/:id', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( removeResponse.err );
            removeResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            removeResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            removeResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with no entries', (done)=> {
            should.not.exist( removeResponse.res.body.length );
            should.not.exist( removeResponse.res.body.id );
            done();
        } );
        it( 'should respond with an empty object', (done)=> {
            JSON.stringify( removeResponse.res.body ).should.equal( JSON.stringify( {} ) );
            done();
        } );
    } );
} );