
process.env.NODE_ENV = 'testing';

const chai = require( 'chai' );
const chaiHTTP = require( 'chai-http' );
const should = chai.should();

chai.use( chaiHTTP );

const server = require( '../../lib/utils/server' );
const test_utils = require( '../utils' );

describe( 'routes : auth', ()=> {

    let registerResponse = undefined;
    let loginResponse = undefined;
    let authenticationResponse = undefined;
    let logoutResponse = undefined;
    
    before( async ()=> {
        await test_utils.DB.setup();

        registerResponse = await test_utils.POST( chai, {
            server: server.app,
            endpoint: '/api/v1/register',
            data: { first_name: 'test-user', last_name: 'one', email: 'tu1@test.com', password: 'asd' }
        });
        loginResponse = await test_utils.POST( chai, {
            server: server.app,
            endpoint: '/api/v1/login',
            data: { email: 'tu1@test.com', password: 'asd' }
        } );
        authenticationResponse = await test_utils.GET_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/authenticate',
            token: loginResponse.res.body.token
        });
        logoutResponse = await test_utils.DELETE_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/logout',
            token: loginResponse.res.body.token
        } );
    } );

    describe( 'POST /api/v1/register', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( registerResponse.err );
            registerResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            registerResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            registerResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with the newly created user', (done)=> {
            should.not.exist( registerResponse.res.body.length );
            should.exist( registerResponse.res.body.id );
            registerResponse.res.body.email.should.equal( 'tu1@test.com' );
            done();
        } );
        it( 'should not respond with the hash/salt of the newly created user', (done)=> {
            registerResponse.res.body.should.include.keys(
                'id', 'date_created', 'date_modified', 'deleted', 'email', 'hash', 'salt', 'profile'
            );
            done();
        } );
    } );

    describe( 'POST /api/v1/login', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( loginResponse.err );
            loginResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            loginResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            loginResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with valid session-data for the provided user', (done)=> {
            should.not.exist( loginResponse.res.body.length );
            should.not.exist( loginResponse.res.body.id );
            done();
        } );
        it( 'should have the correct data-structure', (done)=> {
            loginResponse.res.body.should.include.keys(
                'user', 'token'
            );
            done();
        } );
    } );

    describe( 'GET /api/v1/authenticate', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( authenticationResponse.err );
            authenticationResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            authenticationResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            authenticationResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with a user associated with the token', (done)=> {
            should.not.exist( loginResponse.res.body.length );
            should.not.exist( loginResponse.res.body.id );
            done();
        } );
        it( 'should have the correct data-structure', (done)=> {
            authenticationResponse.res.body.should.include.keys(
                'id', 'date_created', 'date_modified', 'deleted', 'email'
            );
            done();
        } );
    } );

    describe( 'DELETE /api/v1/logout', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( logoutResponse.err );
            logoutResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            logoutResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            logoutResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with no entries', (done)=> {
            should.not.exist( logoutResponse.res.body.length );
            should.not.exist( logoutResponse.res.body.id );
            done();
        } );
        it( 'should respond with an empty object', (done)=> {
            JSON.stringify( logoutResponse.res.body ).should.equal( JSON.stringify( {} ) );
            done();
        } );
    } );
} );