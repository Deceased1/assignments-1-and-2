
/* Always run tests on 'testing' environment */
process.env.NODE_ENV = 'testing';

const chai = require( 'chai' );
const chaiHTTP = require( 'chai-http' );
const should = chai.should();

chai.use( chaiHTTP );

const server = require( '../../lib/utils/server' );
const test_utils = require( '../utils' );

describe( 'routes : profiles', ()=> {

    let countResponse = undefined;
    let getAllResponse = undefined;
    let getByIdResponse = undefined;
    let addResponse = undefined;
    let editResponse = undefined;
    let removeResponse = undefined;
    
    before( async ()=> {
        await test_utils.DB.setup();

        loginResponse = await test_utils.POST( chai, {
            server: server.app,
            endpoint: '/api/v1/login',
            data: { email: 'tu1@test.com', password: 'asd' }
        } );

        countResponse = await test_utils.GET_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/count/profiles',
            token: loginResponse.res.body.token
        } );
        addResponse = await test_utils.POST_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/profiles',
            data: {
                username: 'testUser',
                first_name: 'test',
                last_name: 'user',
                date_of_birth: '1970-01-01',
                country: 'USA',
                gender: 'M',
                contact_numbers: '',
                interests: '',
                user_fk: '00000000-0000-0000-0000-000000000001'
            },
            token: loginResponse.res.body.token
        } );
        getAllResponse = await test_utils.GET_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/profiles',
            token: loginResponse.res.body.token
        });
        getByIdResponse = await test_utils.GET_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/profiles/00000000-0000-0000-0000-000000000001',
            token: loginResponse.res.body.token
        });
        editResponse = await test_utils.PUT_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/profiles/00000000-0000-0000-0000-000000000001',
            data: { contact_numbers: '0000000000' },
            token: loginResponse.res.body.token
        } );
        removeResponse = await test_utils.DELETE_AUTH( chai, {
            server: server.app,
            endpoint: '/api/v1/profiles/00000000-0000-0000-0000-000000000001',
            token: loginResponse.res.body.token
        } );
    } );

    describe( 'GET /api/v1/count/profiles', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( countResponse.err );
            countResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            countResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            countResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with a single profiles entry', (done)=> {
            should.not.exist( countResponse.res.body.length );
            should.exist( countResponse.res.body[ 'count(*)' ] );
            done();
        } );
    } );

    describe( 'POST /api/v1/profiles', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( addResponse.err );
            addResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            addResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            addResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with a single profiles entry', (done)=> {
            should.not.exist( addResponse.res.body.length );
            should.exist( addResponse.res.body.id );
            done();
        } );
        it( 'should have the correct data-structure', (done)=> {
            addResponse.res.body.should.include.keys(
                'id', 'date_created', 'date_modified', 'deleted', 'username',
                'first_name', 'last_name', 'date_of_birth', 'country', 'gender', 'contact_numbers',
                'interests', 'user_fk'
            );
            done();
        } );
    } );

    describe( 'GET /api/v1/profiles', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( getAllResponse.err );
            getAllResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            getAllResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            getAllResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with all the profiles', (done)=> {
            getAllResponse.res.body.length.should.eql( 4 );
            done();
        } );
        it( 'should have the correct data-structure', (done)=> {
            getAllResponse.res.body[ 0 ].should.include.keys(
                'id', 'date_created', 'date_modified', 'deleted', 'username',
                'first_name', 'last_name', 'date_of_birth', 'country', 'gender', 'contact_numbers',
                'interests', 'user_fk'
            );
            done();
        } );
    } );

    describe( 'GET /api/v1/profiles/:id', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( getByIdResponse.err );
            getByIdResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            getByIdResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            getByIdResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with a single profiles entry', (done)=> {
            should.not.exist( getByIdResponse.res.body.length );
            should.exist( getByIdResponse.res.body.id );
            done();
        } );
        it( 'should have the correct data-structure', (done)=> {
            getByIdResponse.res.body.should.include.keys(
                'id', 'date_created', 'date_modified', 'deleted', 'username',
                'first_name', 'last_name', 'date_of_birth', 'country', 'gender', 'contact_numbers',
                'interests', 'user_fk'
            );
            done();
        } );
    } );

    describe( 'PUT /api/v1/profiles/:id', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( editResponse.err );
            editResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            editResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            editResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with a single profiles entry', (done)=> {
            should.not.exist( editResponse.res.body.length );
            should.exist( editResponse.res.body.id );
            done();
        } );
        it( 'should reflect the changes made', (done)=> {
            editResponse.res.body.contact_numbers.should.equal( '0000000000' );
            done();
        } );
    } );

    describe( 'DELETE /api/v1/profiles/:id', ()=> {

        it( 'should not throw any errors', (done)=> {
            should.not.exist( removeResponse.err );
            removeResponse.res.error.should.equal( false );
            done();
        } );
        it( 'should respond with status-code of 200', (done)=> {
            removeResponse.res.status.should.equal( 200 );
            done();
        } );
        it( 'should respond with application/json data', (done)=> {
            removeResponse.res.type.should.equal( 'application/json' );
            done();
        } );
        it( 'should respond with no entries', (done)=> {
            should.not.exist( removeResponse.res.body.length );
            should.not.exist( removeResponse.res.body.id );
            done();
        } );
        it( 'should respond with an empty object', (done)=> {
            JSON.stringify( removeResponse.res.body ).should.equal( JSON.stringify( {} ) );
            done();
        } );
    } );
} );