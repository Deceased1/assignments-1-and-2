
process.env.NODE_ENV = 'testing';

const chai = require( 'chai' );
const expect = chai.expect;

const uuid = require( 'uuid/v4' );

const knex = require( '../../db/knex' );

const Profiles = require( '../../lib/controllers/profiles' );
const test_utils = require( '../utils' );

describe( 'controllers : profiles', ()=> {
    
    let testProfile = {
        id: uuid(),
        date_created: knex.fn.now(),
        date_modified: knex.fn.now(),
        username: 'testUser',
        first_name: 'test',
        last_name: 'user',
        date_of_birth: '1970-01-01',
        country: 'USA',
        gender: 'M',
        contact_numbers: '',
        interests: '',
        user_fk: '00000000-0000-0000-0000-000000000001'
    };

    beforeEach( async ()=> {
        await test_utils.DB.setup();
    } );

    describe( 'count', ()=> {

        const performCount = async (requestQuery={})=> {
            return await Profiles.count( requestQuery );
        };

        it( 'should execute without error', (done)=> {
            expect( performCount ).to.not.throw();
            done();
        } );
        it( 'should return an Object containing the count', async ()=> {
            let record = await performCount();
            expect( record ).to.be.an( 'object' );
        } );
        it( 'should allow filtering', async ()=> {
            let record = await performCount({ query: { username: 'testUser' } });
            expect( record ).to.be.an( 'object' );
        } );
    } );
    
    describe( 'add', ()=> {

        const performInsert = async ()=> {
            return await Profiles.add( testProfile );
        };
        
        it( 'should execute without error', (done)=> {
            expect( performInsert ).to.not.throw();
            done();
        } );
        it( 'should return the newly-created record', async ()=> {
            let record = await performInsert();
            expect( record ).to.be.an( 'object' );
        } );
        it( 'should return a record with the correct data-structure', async ()=> {
            let record = await performInsert();
            expect( record.id ).to.exist;
            expect( record.date_created ).to.exist;
            expect( record.date_modified ).to.exist;
            expect( record.deleted ).to.exist;
            expect( record.username ).to.exist;
            expect( record.first_name ).to.exist;
            expect( record.last_name ).to.exist;
            expect( record.date_of_birth ).to.exist;
            expect( record.country ).to.exist;
            expect( record.gender ).to.exist;
            expect( record.contact_numbers ).to.exist;
            expect( record.interests ).to.exist;
            expect( record.user_fk ).to.exist;
        } );
    } );

    describe( 'getAll', ()=> {
        
        const performRead = async ()=> {
            return await Profiles.getAll();
        };

        it( 'should execute without error', (done)=> {
            expect( performRead ).to.not.throw();
            done();
        } );
        it( 'should return an array containing all the records', async ()=> {
            let records = await performRead();
            expect( records ).to.be.an( 'array' );
        } );
        it( 'should contain elements with the correct data-structure', async ()=> {
            let records = await performRead();
            let record = records[ test_utils.RANDOM( 0, ( records.length - 1 ) ) ];
            expect( record.id ).to.exist;
            expect( record.date_created ).to.exist;
            expect( record.date_modified ).to.exist;
            expect( record.deleted ).to.exist;
            expect( record.username ).to.exist;
            expect( record.first_name ).to.exist;
            expect( record.last_name ).to.exist;
            expect( record.date_of_birth ).to.exist;
            expect( record.country ).to.exist;
            expect( record.gender ).to.exist;
            expect( record.contact_numbers ).to.exist;
            expect( record.interests ).to.exist;
            expect( record.user_fk ).to.exist;
        } );
    } );

    describe( 'getById', ()=> {
        
        const performRead = async ()=> {
            return await Profiles.getById( '00000000-0000-0000-0000-000000000001' );
        };

        it( 'should execute without error', (done)=> {
            expect( performRead ).to.not.throw();
            done();
        } );
        it( 'should return a single object', async ()=> {
            let record = await performRead();
            expect( record ).to.be.an( 'object' );
        } );
        it( 'should return a record with the correct data-structure', async ()=> {
            let record = await performRead();
            expect( record.id ).to.exist;
            expect( record.date_created ).to.exist;
            expect( record.date_modified ).to.exist;
            expect( record.deleted ).to.exist;
            expect( record.username ).to.exist;
            expect( record.first_name ).to.exist;
            expect( record.last_name ).to.exist;
            expect( record.date_of_birth ).to.exist;
            expect( record.country ).to.exist;
            expect( record.gender ).to.exist;
            expect( record.contact_numbers ).to.exist;
            expect( record.interests ).to.exist;
            expect( record.user_fk ).to.exist;
        } );
    } );

    describe( 'edit', ()=> {

        const performEdit = async ()=> {
            return await Profiles.edit( '00000000-0000-0000-0000-000000000001', { id: '123', username: 'test' } );
        };

        it( 'should execute without error', (done)=> {
            expect( performEdit ).to.not.throw();
            done();
        } );
        it( 'should return the modified record', async ()=> {
            let record = await performEdit();
            expect( record ).to.be.an( 'object' );
        } );
        it( 'should apply the updates to the specified record', async ()=> {
            let record = await performEdit();
            expect( record.username ).to.be.equal( 'test' );
        } );
        it( 'should return a record with the correct data-structure', async ()=> {
            let record = await performEdit();
            expect( record.id ).to.exist;
            expect( record.date_created ).to.exist;
            expect( record.date_modified ).to.exist;
            expect( record.deleted ).to.exist;
            expect( record.username ).to.exist;
            expect( record.first_name ).to.exist;
            expect( record.last_name ).to.exist;
            expect( record.date_of_birth ).to.exist;
            expect( record.country ).to.exist;
            expect( record.gender ).to.exist;
            expect( record.contact_numbers ).to.exist;
            expect( record.interests ).to.exist;
            expect( record.user_fk ).to.exist;
        } );
        it( 'should discard change-attempts made to the record-id', async ()=> {
            let record = await performEdit();
            expect( record.id ).to.be.equal( '00000000-0000-0000-0000-000000000001' );
        } );
    } );

    describe( 'remove', ()=> {

        const performRemove = async ()=> {
            return await Profiles.remove( '00000000-0000-0000-0000-000000000001' );
        };

        it( 'should execute without error', (done)=> {
            expect( performRemove ).to.not.throw();
            done();
        } );
        it( 'should return the removed record', async ()=> {
            let record = await performRemove();
            expect( record ).to.be.an( 'object' );
        } );
        it( 'should return a record with the correct data-structure', async ()=> {
            let record = await performRemove();
            expect( record.id ).to.exist;
            expect( record.date_created ).to.exist;
            expect( record.date_modified ).to.exist;
            expect( record.deleted ).to.exist;
            expect( record.username ).to.exist;
            expect( record.first_name ).to.exist;
            expect( record.last_name ).to.exist;
            expect( record.date_of_birth ).to.exist;
            expect( record.country ).to.exist;
            expect( record.gender ).to.exist;
            expect( record.contact_numbers ).to.exist;
            expect( record.interests ).to.exist;
            expect( record.user_fk ).to.exist;
        } );
        it( 'should not delete the record from the DB', async ()=> {
            let record = await performRemove();
            expect( record.deleted ).to.be.equal( 1 );
        } );
    } );
} );