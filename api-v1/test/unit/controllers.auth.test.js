
process.env.NODE_ENV = 'testing';

const chai = require( 'chai' );
const expect = chai.expect;

const Auth = require( '../../lib/controllers/auth' );
const test_utils = require( '../utils' );

describe( 'controllers : auth', ()=> {

    before( async ()=> {
        await test_utils.DB.setup();
    } );

    describe( 'register', ()=> {

        const performRegister = async (requestQuery={})=> {
            return await Auth.register({ first_name: 'test-user', last_name: 'one', email: 'tu@one.com', password: 'asd' });
        };

        it( 'should execute without error', (done)=> {
            expect( performRegister ).to.not.throw();
            done();
        } );
        it( 'should return the newly-created record', async ()=> {
            let record = await performRegister();
            expect( record ).to.be.an( 'object' );
        } );
        it( 'should return a record with the correct data-structure', async ()=> {
            let record = await performRegister();
            expect( record.id ).to.exist;
            expect( record.date_created ).to.exist;
            expect( record.date_modified ).to.exist;
            expect( record.deleted ).to.exist;
            expect( record.email ).to.exist;
        } );
    } );

    describe( 'login', ()=> {

        const performLogin = async (requestQuery={})=> {
            return await Auth.login({ email: 'tu1@test.com', password: 'asd' });
        };

        it( 'should execute without error', (done)=> {
            expect( performLogin ).to.not.throw();
            done();
        } );
        it( 'should return the newly-created record', async ()=> {
            let record = await performLogin();
            expect( record ).to.be.an( 'object' );
        } );
        it( 'should return a record with the correct data-structure', async ()=> {
            let record = await performLogin();
            expect( record.user.id ).to.exist;
            expect( record.user.date_created ).to.exist;
            expect( record.user.date_modified ).to.exist;
            expect( record.user.deleted ).to.exist;
            expect( record.user.email ).to.exist;
            expect( record.token.id ).to.exist;
            expect( record.token.key ).to.exist;
            expect( record.session.id ).to.exist;
            expect( record.session.date_created ).to.exist;
            expect( record.session.date_modified ).to.exist;
            expect( record.session.deleted ).to.exist;
            expect( record.session.token_id ).to.exist;
            expect( record.session.signed_out ).to.exist;
            expect( record.session.user_fk ).to.exist;
        } );
    } );

    describe( 'logout', ()=> {

        const performLogout = async (requestQuery={})=> {
            return await Auth.logout({
                id: '00000000-0000-0000-0000-000000000001',
                email: 'tu1@test.com',
                hash: '$2a$04$r3rDryPadurKQCh2twgHXeVQjXHavfS8SycS3XSBmr413q9abF8Ri',
                salt: '$2a$12$cG9JZT8UrEmo0dThzh.OJO'
            });
        };

        it( 'should execute without error', (done)=> {
            expect( performLogout ).to.not.throw();
            done();
        } );
    } );
} );
