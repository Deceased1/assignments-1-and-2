
process.env.NODE_ENV = 'testing';

const chai = require( 'chai' );
const expect = chai.expect;

const uuid = require( 'uuid/v4' );

const knex = require( '../../db/knex' );

const Contacts = require( '../../lib/controllers/contacts' );
const test_utils = require( '../utils' );

describe( 'controllers : contacts', ()=> {
    
    let testContact = {
        id: uuid(),
        date_created: knex.fn.now(),
        date_modified: knex.fn.now(),
        deleted: false,
        first_name: 'test',
        last_name: 'contact',
        work: 'tester',
        phone: '',
        email: 'tester@testing.com',
        user_fk: '00000000-0000-0000-0000-000000000001'
    };

    beforeEach( async ()=> {
        await test_utils.DB.setup();
    } );

    describe( 'count', ()=> {

        const performCount = async (requestQuery={})=> {
            return await Contacts.count( requestQuery );
        };

        it( 'should execute without error', (done)=> {
            expect( performCount ).to.not.throw();
            done();
        } );
        it( 'should return an Object containing the count', async ()=> {
            let record = await performCount();
            expect( record ).to.be.an( 'object' );
        } );
        it( 'should allow filtering', async ()=> {
            let record = await performCount({ query: { first_name: 'test' } });
            expect( record ).to.be.an( 'object' );
        } );
    } );
    
    describe( 'add', ()=> {

        const performInsert = async ()=> {
            return await Contacts.add( testContact );
        };
        
        it( 'should execute without error', (done)=> {
            expect( performInsert ).to.not.throw();
            done();
        } );
        it( 'should return the newly-created record', async ()=> {
            let record = await performInsert();
            expect( record ).to.be.an( 'object' );
        } );
        it( 'should return a record with the correct data-structure', async ()=> {
            let record = await performInsert();
            expect( record.id ).to.exist;
            expect( record.date_created ).to.exist;
            expect( record.date_modified ).to.exist;
            expect( record.deleted ).to.exist;
            expect( record.first_name ).to.exist;
            expect( record.last_name ).to.exist;
            expect( record.work ).to.exist;
            expect( record.phone ).to.exist;
            expect( record.email ).to.exist;
            expect( record.user_fk ).to.exist;
        } );
    } );

    describe( 'getAll', ()=> {
        
        const performRead = async ()=> {
            return await Contacts.getAll();
        };

        it( 'should execute without error', (done)=> {
            expect( performRead ).to.not.throw();
            done();
        } );
        it( 'should return an array containing all the records', async ()=> {
            let records = await performRead();
            expect( records ).to.be.an( 'array' );
        } );
        it( 'should contain elements with the correct data-structure', async ()=> {
            let records = await performRead();
            let record = records[ test_utils.RANDOM( 0, ( records.length - 1 ) ) ];
            expect( record.id ).to.exist;
            expect( record.date_created ).to.exist;
            expect( record.date_modified ).to.exist;
            expect( record.deleted ).to.exist;
            expect( record.first_name ).to.exist;
            expect( record.last_name ).to.exist;
            expect( record.work ).to.exist;
            expect( record.phone ).to.exist;
            expect( record.email ).to.exist;
            expect( record.user_fk ).to.exist;
        } );
    } );

    describe( 'getById', ()=> {
        
        const performRead = async ()=> {
            return await Contacts.getById( '00000000-0000-0000-0000-000000000001' );
        };

        it( 'should execute without error', (done)=> {
            expect( performRead ).to.not.throw();
            done();
        } );
        it( 'should return a single object', async ()=> {
            let record = await performRead();
            expect( record ).to.be.an( 'object' );
        } );
        it( 'should return a record with the correct data-structure', async ()=> {
            let record = await performRead();
            expect( record.id ).to.exist;
            expect( record.date_created ).to.exist;
            expect( record.date_modified ).to.exist;
            expect( record.deleted ).to.exist;
            expect( record.first_name ).to.exist;
            expect( record.last_name ).to.exist;
            expect( record.work ).to.exist;
            expect( record.phone ).to.exist;
            expect( record.email ).to.exist;
            expect( record.user_fk ).to.exist;
        } );
    } );

    describe( 'edit', ()=> {

        const performEdit = async ()=> {
            return await Contacts.edit( '00000000-0000-0000-0000-000000000001', { id: '123', phone: '0000000000' } );
        };

        it( 'should execute without error', (done)=> {
            expect( performEdit ).to.not.throw();
            done();
        } );
        it( 'should return the modified record', async ()=> {
            let record = await performEdit();
            expect( record ).to.be.an( 'object' );
        } );
        it( 'should apply the updates to the specified record', async ()=> {
            let record = await performEdit();
            expect( record.phone ).to.be.equal( '0000000000' );
        } );
        it( 'should return a record with the correct data-structure', async ()=> {
            let record = await performEdit();
            expect( record.id ).to.exist;
            expect( record.date_created ).to.exist;
            expect( record.date_modified ).to.exist;
            expect( record.deleted ).to.exist;
            expect( record.first_name ).to.exist;
            expect( record.last_name ).to.exist;
            expect( record.work ).to.exist;
            expect( record.phone ).to.exist;
            expect( record.email ).to.exist;
            expect( record.user_fk ).to.exist;
        } );
        it( 'should discard change-attempts made to the record-id', async ()=> {
            let record = await performEdit();
            expect( record.id ).to.be.equal( '00000000-0000-0000-0000-000000000001' );
        } );
    } );

    describe( 'remove', ()=> {

        const performRemove = async ()=> {
            return await Contacts.remove( '00000000-0000-0000-0000-000000000001' );
        };

        it( 'should execute without error', (done)=> {
            expect( performRemove ).to.not.throw();
            done();
        } );
        it( 'should return the removed record', async ()=> {
            let record = await performRemove();
            expect( record ).to.be.an( 'object' );
        } );
        it( 'should return a record with the correct data-structure', async ()=> {
            let record = await performRemove();
            expect( record.id ).to.exist;
            expect( record.date_created ).to.exist;
            expect( record.date_modified ).to.exist;
            expect( record.deleted ).to.exist;
            expect( record.first_name ).to.exist;
            expect( record.last_name ).to.exist;
            expect( record.work ).to.exist;
            expect( record.phone ).to.exist;
            expect( record.email ).to.exist;
            expect( record.user_fk ).to.exist;
        } );
        it( 'should not delete the record from the DB', async ()=> {
            let record = await performRemove();
            expect( record.deleted ).to.be.equal( 1 );
        } );
    } );
} );