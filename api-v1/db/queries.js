
const User = require( '../lib/models/user' );
const Session = require( '../lib/models/session' );
const Profile = require( '../lib/models/profile' );
const Contact = require( '../lib/models/contact.js' );

/* ***************************************			Auth 			*************************************** */

const readUserByEmail = (email)=> {
	return User.query().findOne({ email: email }).where( `${ User.tableName }.deleted`, false );
};
const readUserByUsername = async (username)=> {
	let profile = await Profile.query().findOne({ username: username }).where( `${ Profile.tableName }.deleted`, false );
	return profile.$relatedQuery().where( `${ User.tableName }.deleted`, false );
};

/* ***************************************			Sessions		*************************************** */

const readSessionById = (id)=> {
	return Session.query().findOne({ id: id }).where( `${ Session.tableName }.deleted`, false );
};

const readSessionByUserId = (user_id)=> {
	return Session.query().findOne({ user_fk: user_id }).where( `${ Session.tableName }.deleted`, false );
};

const createSession = (data)=> {
	return Session.query().insertAndFetch( data );
};

const updateSession = (id, updates)=> {
	return Session.query().updateAndFetchById( id, updates );
};

/* ***************************************			Users 			*************************************** */

const readAllUsers = ()=> {
	return User.query().where( `${ User.tableName }.deleted`, false );
};

const readUserById = (id)=> {
	return User.query().findOne({ id: id }).where( `${ User.tableName }.deleted`, false );
};

const createUser = (data)=> {
	return User.query().insertAndFetch( data );
};

const updateUser = (id, updates)=> {
	return User.query().updateAndFetchById( id, updates );
};

const deleteUser = (id)=> {
	return User.query().updateAndFetchById( id, { deleted: true } );
};

/* ***************************************		    Profiles 		*************************************** */
const readAllProfiles = ()=> {
	return Profile.query().where( `${ Profile.tableName }.deleted`, false );
};

const readProfileByUserId = (id)=> {
	return Profile.query().findOne({ user_fk: id }).where( `${ Profile.tableName }.deleted`, false );
};

const createProfile = (data)=> {
	return Profile.query().insertAndFetch( data );
};

const updateProfile = (id, updates)=> {
	return Profile.query().updateAndFetchById( id, updates );
};

const deleteProfile = (id)=> {
	return Profile.query().updateAndFetchById( id, { deleted: true } );
};

/* ***************************************			Contacts 		*************************************** */
const readAllContact = ()=> {
	return Contact.query().where( `${ Contact.tableName }.deleted`, false );
};

const readContactById = (id)=> {
	return Contact.query().findOne({ id: id }).where( `${ Contact.tableName }.deleted`, false );
};

const createContact = (data)=> {
	return Contact.query().insertAndFetch( data );
};

const updateContact = (id, updates)=> {
	return Contact.query().updateAndFetchById( id, updates );
};

const deleteContact = (id)=> {
	return Contact.query().updateAndFetchById( id, { deleted: true } );
};

module.exports = {
	Auth: {
		readByEmail: readUserByEmail,
		readByUsername: readUserByUsername
	},
	Session: {
		readById: readSessionById,
		readByUserId: readSessionByUserId,
		create: createSession,
		update: updateSession,
	},
	Users: {
		readAll: readAllUsers,
		readById: readUserById,
		create: createUser,
		update: updateUser,
		delete: deleteUser
	},
	Profiles: {
		readAll: readAllProfiles,
		readByUserId: readProfileByUserId,
		create: createProfile,
		update: updateProfile,
		delete: deleteProfile
	},
	Contacts: {
		readAll: readAllContact,
		readById: readContactById,
		create: createContact,
		update: updateContact,
		delete: deleteContact
	},
};