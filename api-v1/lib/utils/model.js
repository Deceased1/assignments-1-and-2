
const uuid = require( 'uuid/v4' );

const { Model } = require( 'objection' );
const knex = require( '../../db/knex' );

class ExtendedModel extends Model {
    $beforeInsert() {
        this.id = uuid();
        this.date_created = new Date().toISOString();
    }
    
    $beforeUpdate() {
        this.date_modified = new Date().toISOString();
    }
    
    static get idColumn() {
        return 'id';
    }
}

ExtendedModel.knex( knex );

module.exports = ExtendedModel;