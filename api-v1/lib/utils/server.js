
const express = require( 'express' );
const bodyParser = require( 'body-parser' );
const cors = require( 'cors' );

const morgan = require( 'morgan' );

const config = require( './config' );
const routes = require( '../routes' );

class Server {
    constructor() {
        this.app = express();

        this.app.use( bodyParser.urlencoded({ extended: true }) );
        this.app.use( bodyParser.json() );
        this.app.use( cors({ optionsSuccessStatus: 200, }) );

        /* Turn off Logging for 'testing' environment */
        if ( process.env.NODE_ENV !== 'testing' ) {
            this.app.use( morgan( 'dev' ) );
        }

        this.app.use( (req, res, next)=> {
            res.setHeader( 'content-type', 'application/json' );
            res.status( 200 );
            next();
        } );

        this.app.get( '/status', (req, res)=> {
            res.send( `${ config.service.name } is running...` );
            res.end();
        } );

        this.app.use( `${ config.service.endpoint }`, routes );

        /* Do not send Error information for non- 'development' environments */
        if ( this.app.get( 'env' ) === 'development' ) {
            this.app.use( (err, req, res, next)=> {
                res.status( err.status || 500 );
                res.json({
                    message: err.message,
                    error: err
                });
            } );
        } else {
            this.app.use( (err, req, res, next)=> {
                res.status( err.status || 500 );
                res.json({
                    message: err.message,
                    error: {}
                });
            } );
        }
    }

    start() {
        this.service = this.app.listen( config.service.port, ()=> {
            console.log( `${ config.service.name } listening on http://${ config.service.internalIP }:${ config.service.port }` );
        } );
    }

    stop() {
        this.service.close();
    }
}

const server = new Server( config );
module.exports = server;