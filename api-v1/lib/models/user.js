
const BCrypt = require( 'bcrypt-nodejs' );
const jsonWebToken = require( 'jsonwebtoken' );
const uuid = require( 'uuid/v4' );

const Model = require( '../utils/model' );

class User extends Model {

    static get tableName() {
        return '2es_user';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: [],
            properties: {
                id: { type: 'uuid' },
                date_created: { type: 'string' },
                date_modified: { type: 'string' },
                deleted: { tye: 'boolean' },
                email: { type: 'string' },
                hash: { type: 'string' },
                salt: { type: 'string' }
            }
        }
    }

    static get relationMappings() {
        const Contact = require( './contact' );
        const Profile = require( './profile' );
        const Session = require( './session' );
        return {
            contacts: {
                relation: Model.HasManyRelation,
                modelClass: Contact,
                join: {
                    from: '2es_contact.user_fk',
                    to: '2es_user.id'
                }
            },
            profile: {
                relation: Model.HasOneRelation,
                modelClass: Profile,
                join: {
                    from: '2es_profile.user_fk',
                    to: '2es_user.id'
                }
            },
            session: {
                relation: Model.HasOneRelation,
                modelClass: Session,
                join: {
                    from: '2es_session.user_fk',
                    to: '2es_user.id'
                }
            }
        };
    }

    $formatJson( json ) {
        json = super.$formatJson( json );

        delete json.hash;
        delete json.salt;
        return json;
    }

    validatePassword( password ) {
        return new Promise( (resolve, reject)=> {
            BCrypt.compare( password, this.hash, (err, result)=> {
                if ( err ) { reject( err ); }
                resolve( result );
            })
        });
    }

    generateJWT( email ) {
        const options = { expiresIn: 86400 };
        let id = uuid();
        return {
            id: id,
            key: jsonWebToken.sign( { email, id: id }, process.env.SESSION_SECRET, options )
        };
    }
}

module.exports = User;