
const Model = require( '../utils/model' );

class Contact extends Model {

    static get tableName() {
        return '2es_contact';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: [],
            properties: {
                id: { type: 'uuid' },
                date_created: { type: 'string' },
                date_modified: { type: 'string' },
                deleted: { tye: 'boolean' },
                first_name: { type: 'string' },
                last_name: { type: 'string' },
                work: { type: 'string' },
                phone: { type: 'string' },
                email: { type: 'string' },
                user_fk: { type: 'uuid' }
            }
        }
    }

    static get relationMappings() {
        const User = require( './user' );
        return {
            user: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: '2es_user.id',
                    to: '2es_contact.user_fk'
                }
            }
        }
    };
}

module.exports = Contact;