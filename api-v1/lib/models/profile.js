
const Model = require( '../utils/model' );

class Profile extends Model {

    static get tableName() {
        return '2es_profile';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: [],
            properties: {
                id: { type: 'uuid' },
                date_created: { type: 'string' },
                date_modified: { type: 'string' },
                deleted: { tye: 'boolean' },
                username: { type: 'string' },
                first_name: { type: 'string' },
                last_name: { type: 'string' },
                date_of_birth: { type: 'string' },
                country: { type: 'string' },
                gender: { type: 'string' },
                contact_numbers: { type: 'string' },
                interests: { type: 'string' },
                user_fk: { type: 'uuid' }
            }
        }
    }

    static get relationMappings() {
        const User = require( './user' );
        return {
            user: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: '2es_user.id',
                    to: '2es_profile.user_fk'
                }
            }
        }
    };
}

module.exports = Profile;