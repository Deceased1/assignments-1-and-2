
const Model = require( '../utils/model' );

const User = require( './user' );

class Session extends Model {

    static get tableName() {
        return '2es_session';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: [],
            properties: {
                id: { type: 'uuid' },
                date_created: { type: 'string' },
                date_modified: { type: 'string' },
                deleted: { type: 'boolean' },
                token_id: { type: 'uuid' },
                signed_out: { type: 'boolean' },
                user_fk: { type: 'uuid' }
            }
        }
    }

    static get relationMappings() {
        return {
            user: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: '2es_session.user_fk',
                    to: '2es_user.id'
                }
            }
        };
    }
}

module.exports = Session;