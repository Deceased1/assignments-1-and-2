
const express = require( 'express' );
const router = express.Router();

const passport = require( 'passport' );
const JwtStrategy = require( 'passport-jwt' ).Strategy;
const ExtractJwt = require( 'passport-jwt' ).ExtractJwt;

const queries = require( '../../db/queries' );
const controllers = require( '../controllers' );

const strategyOptions = {
	secretOrKey: process.env.SESSION_SECRET,
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};

passport.use( new JwtStrategy( strategyOptions, async (token, done) => {
	
	// Determine whether the session is signed-out.
	let session = await queries.Session.readById( token.id );
	if ( session ) {
		if ( session.signed_out ) {
			return done( null, false );
		}
	}

	// Retun either 'false' or the user as the result;
	let response = false;
	let potentialUser = await queries.Auth.readByEmail( token.email );
	if ( potentialUser ) {
		response = potentialUser;
	}
	return done( null, response );
} ) );

/* ***************************************			Auth		    *************************************** */
router.post( '/register', async (req, res, next)=> {
	try {
		let user = await controllers.Auth.register( req.body );
		res.status( 200 ).json( user );
	} catch( err ) {
		next( err );
	}
} );

router.post( '/login', async (req, res, next)=> {
	try {
		let session = await controllers.Auth.login( req.body );
		res.status( 200 ).json({ user: session.user, token: session.token.key });
	} catch( err ) {
		next( err );
	}
} );

router.get( '/authenticate', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		res.status( 200 ).json( req.user );
	} catch( err ) {
		next( err );
	}
} );

router.delete( '/logout', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		await controllers.Auth.logout( req.user );
		res.status( 200 ).json( {} );
	} catch( err ) {
		next( err );
	}
} );

/* ***************************************			Users		    *************************************** */
router.get( '/count/users', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let users = await controllers.Users.count( req );
		res.status( 200 ).json( users );
	} catch( err ) {
		next( err );
	}
} );
router.get( '/users', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let users = await controllers.Users.getAll();
		res.status( 200 ).json( users );
	} catch( err ) {
		next( err );
	}
} );
router.get( '/users/:id', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let user = await controllers.Users.getById( req.params.id );
		res.status( 200 ).json( user );
	} catch( err ) {
		next( err );
	}
} );
router.post( '/users', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let user = await controllers.Users.add( req.body );
		res.status( 200 ).json( user );
	} catch( err ) {
		next( err );
	}
} );
router.put( '/users/:id', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let user = await controllers.Users.edit( req.params.id, req.body );
		res.status( 200 ).json( user );
	} catch( err ) {
		next( err );
	}
} );
router.delete( '/users/:id', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		await controllers.Users.remove( req.params.id );
		res.status( 200 ).json( {} );
	} catch( err ) {
		next( err );
	}
} );


/* ***************************************			Profiles		*************************************** */
router.get( '/count/profiles', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let profiles = await controllers.Profiles.count( req );
		res.status( 200 ).json( profiles );
	} catch( err ) {
		next( err );
	}
} );
router.get( '/profiles', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let profiles = await controllers.Profiles.getAll();
		res.status( 200 ).json( profiles );
	} catch( err ) {
		next( err );
	}
} );
router.get( '/profiles/:id', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let profile = await controllers.Profiles.getByUserId( req.params.id );
		res.status( 200 ).json( profile );
	} catch( err ) {
		next( err );
	}
} );
router.post( '/profiles', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let profile = await controllers.Profiles.add( req.body );
		res.status( 200 ).json( profile );
	} catch( err ) {
		next( err );
	}
} );
router.put( '/profiles/:id', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let profile = await controllers.Profiles.edit( req.params.id, req.body );
		res.status( 200 ).json( profile );
	} catch( err ) {
		next( err );
	}
} );
router.delete( '/profiles/:id', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		await controllers.Profiles.remove( req.params.id );
		res.status( 200 ).json( {} );
	} catch( err ) {
		next( err );
	}
} );

/* ***************************************		Contacts			*************************************** */
router.get( '/count/contacts', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let contacts = await controllers.Contacts.count( req );
		res.status( 200 ).json( contacts );
	} catch( err ) {
		next( err );
	}
} );
router.get( '/contacts', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let contacts = await controllers.Contacts.getAll();
		res.status( 200 ).json( contacts );
	} catch( err ) {
		next( err );
	}
} );
router.get( '/contacts/:id', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let contact = await controllers.Contacts.getById( req.params.id );
		res.status( 200 ).json( contact );
	} catch( err ) {
		next( err );
	}
} );
router.post( '/contacts', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let contact = await controllers.Contacts.add( req.body );
		res.status( 200 ).json( contact );
	} catch( err ) {
		next( err );
	}
} );
router.put( '/contacts/:id', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		let contact = await controllers.Contacts.edit( req.params.id, req.body );
		res.status( 200 ).json( contact );
	} catch( err ) {
		next( err );
	}
} );
router.delete( '/contacts/:id', passport.authenticate( 'jwt', { session: false } ), async (req, res, next)=> {
	try {
		await controllers.Contacts.remove( req.params.id );
		res.status( 200 ).json( {} );
	} catch( err ) {
		next( err );
	}
} );

module.exports = router;