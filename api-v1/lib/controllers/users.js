
const queries = require( '../../db/queries' );

module.exports = {
    count: (req)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Users.readAll();
                let result = baseQuery.where({ ...req.query }).count().first();

                resolve( await result );
            } catch( err ) {
                reject( err );
            }
        } );
    },
    getAll: ()=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Users.readAll();

                resolve( await baseQuery );
            } catch( err ) {
                reject( err );
            }
        } );
    },
    getById: (id)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Users.readById( id );

                resolve( await baseQuery );
            } catch( err ) {
                reject( err );
            }
        } );
    },
    add: (data)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Users.create( data );

                resolve( await baseQuery );
            } catch( err ) {
                console.log( err );
                reject( err );
            }
        } );
    },
    edit: (id, updates)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                if ( updates.hasOwnProperty( 'id' ) ) {
                    delete updates.id;
                }

                let baseQuery = queries.Users.update( id, updates );

                resolve( await baseQuery );
            } catch( err ) {
                reject( err );
            }
        } );
    },
    remove: (id)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Users.delete( id );

                resolve( await baseQuery );
            } catch( err ) {
                reject( err );
            }
        } );
    }
};