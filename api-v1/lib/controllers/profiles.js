
const queries = require( '../../db/queries' );

module.exports = {
    count: (req)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Profiles.readAll();
                let result = baseQuery.where({ ...req.query }).count().first();

                resolve( await result );
            } catch( err ) {
                reject( err );
            }
        } );
    },
    getAll: ()=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Profiles.readAll();

                resolve( await baseQuery );
            } catch( err ) {
                reject( err );
            }
        } );
    },
    getByUserId: (id)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Profiles.readByUserId( id )
                    .eager( '[ user ]' )
                    .modifyEager( 'user', (builder)=> ( builder.where( '2es_user.deleted', false ) ) )

                resolve( await baseQuery );
            } catch( err ) {
                reject( err );
            }
        } );
    },
    add: (data)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Profiles.create( data );

                resolve( await baseQuery );
            } catch( err ) {
                console.log( err );
                reject( err );
            }
        } );
    },
    edit: (id, updates)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                if ( updates.hasOwnProperty( 'id' ) ) {
                    delete updates.id;
                }

                let baseQuery = queries.Profiles.update( id, updates );

                resolve( await baseQuery );
            } catch( err ) {
                reject( err );
            }
        } );
    },
    remove: (id)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Profiles.delete( id );

                resolve( await baseQuery );
            } catch( err ) {
                reject( err );
            }
        } );
    }
};