
const queries = require( '../../db/queries' );

module.exports = {
    count: (req)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Contacts.readAll();
                let result = baseQuery.where({ ...req.query }).count().first();

                resolve( await result );
            } catch( err ) {
                reject( err );
            }
        } );
    },
    getAll: ()=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Contacts.readAll();

                resolve( await baseQuery );
            } catch( err ) {
                reject( err );
            }
        } );
    },
    getById: (id)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Contacts.readById( id );

                resolve( await baseQuery );
            } catch( err ) {
                reject( err );
            }
        } );
    },
    add: (data)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Contacts.create( data );

                resolve( await baseQuery );
            } catch( err ) {
                console.log( err );
                reject( err );
            }
        } );
    },
    edit: (id, updates)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                if ( updates.hasOwnProperty( 'id' ) ) {
                    delete updates.id;
                }

                let baseQuery = queries.Contacts.update( id, updates );

                resolve( await baseQuery );
            } catch( err ) {
                reject( err );
            }
        } );
    },
    remove: (id)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let baseQuery = queries.Contacts.delete( id );

                resolve( await baseQuery );
            } catch( err ) {
                reject( err );
            }
        } );
    }
};