
const BCrypt = require( 'bcrypt-nodejs' );

const queries = require( '../../db/queries' );

const encrypt = (password)=> {
	return new Promise( (resolve, reject)=> {
		BCrypt.genSalt( process.env.SALT_ROUNDS, (err, salt)=> {
			if ( err ) { reject( err ); }
			BCrypt.hash( password, salt, null, ( err, hash )=> {
				if ( err ) { reject( err ); }
				resolve({ salt, hash });
			} );
		} );
	} );
};

module.exports = {
    register: (data)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let encryption = await encrypt( data.password );
        
                let user = await queries.Users.create({
                    email: data.email,
                    salt: encryption.salt,
                    hash: encryption.hash
                });
        
                let profile = await queries.Profiles.create({
                    username: data.username,
                    first_name: data.first_name,
                    last_name: data.last_name,
                    date_of_birth: data.date_of_birth,
                    contact_numbers: data.contact_numbers,
                    interests: data.interests,
                    user_fk: user.id
                });

                resolve({ ...user, profile: profile });
            } catch( err ) {
                reject( err );
            }
        } );
    },
    login: (data)=> {
        return new Promise( async (resolve, reject)=> {

            // Try logging in with an email-address:
            let potentialUser = await queries.Auth.readByEmail( data.username );
            // if the potential user is still undefined at this point, try loggin in with profile-username:
            if ( !potentialUser ) {
                potentialUser = await queries.Auth.readByUsername( data.username );
            }

            if ( potentialUser ) {
                let match = await potentialUser.validatePassword( data.password );
                if ( match ) {
                    let session = await queries.Session.readByUserId( potentialUser.id );
                    const token = potentialUser.generateJWT( potentialUser.email );
                    if ( !session ) {
                        session = await queries.Session.create({ token_id: token.id, signed_out: false, user_fk: potentialUser.id });
                    }

                    /* Remove any Auth Fields */
                    delete potentialUser.hash;
                    delete potentialUser.salt;

                    resolve({ user: potentialUser, session: session, token });
                }
            }
            reject( 'Invalid username or password' );
        } );
    },
    logout: (requestUser)=> {
        return new Promise( async (resolve, reject)=> {
            try {
                let session = await queries.Session.readByUserId( requestUser.id );
                await queries.Session.update( session.id, { signed_out: true } );
                resolve( {} );
            } catch( err ) {
                reject( err );
            }
        } );
    }
};