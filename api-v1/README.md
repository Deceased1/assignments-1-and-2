
# API - V1

This is a RESTFul service to interact with the database.

It is separated into the following facets:
* Server -> Router -> Controllers -> Models

Please note, the project assumes the existence of the corresponding database ( and tables on initial run ).

## Configuration

This project runs off a .env ( dotenv ) configuration and Convict.

To create a dotenv file, in the project root:
```
touch .env
```

This dotenv file needs at least the following values filled out:
* NODE_ENV={ testing | production | development | staging }
* DATABASE_USERNAME={ *db_user_name* }
* DATABASE_PASSWORD={ *db_user_password* }
* DATABASE_HOST={ *db_address* }

To support testing, the following value needs to also exist:
* KNEX_WRAPPER={ *absolute_project_directory* }

### Testing

Tests can be run with the npm-command ```npm test```.

The testing for this project is set up in such a way that the above command will execute:
* unit-tests
* integration-tests
in that order. Should any test fail, the results are displayed after all tests for that suite are run.

Running tests will execute node with the ```testing``` environment - this means that the provided KNEX_WRAPPER project needs to know how to configure this database ( follow the instructions in ```../databases/README.md``` ).