
const config = require( './lib/utils/config' );

const getConnection = ()=> {
    return {
        user: config.db.username,
        password: config.db.password,
        database: config.db.database
    };
};

module.exports = {
	testing: {
		client: 'mysql',
		connection: getConnection(),
		pool: { min: 0, max: 7 }
	},
	development: {
		client: 'mysql',
		connection: getConnection(),
		pool: { min: 0, max: 7 }
	},
	staging: {
		client: 'mysql',
		connection: getConnection(),
		pool: { min: 0, max: 7 }
	},
	production: {
		client: 'mysql',
		connection: getConnection(),
		pool: { min: 0, max: 7 }
	}
};