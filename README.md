
# Assignments 1 and 2

This project covers both assignments 1 and 2, combining the Registration Form and the Phonebook app. The system is built in a very loosely-coupled fashion, consisting of a Database, a RESTful API backend and a React frontend.

* databases
* api-v1
* www

The technologies used for each section is as follows:

### Databases

The requirement is to build on a MySQL instance. This needs to be taken into consideration for the technologies used by the backend.

### API-V1

The backend will be built in Node.js. The packages utilized will include:

* knex
* ObjectionJS
* mysql
* express
* body-parser
* cors
* morgan
* dotenv
* convict
* mocha
* chai
* chai-http
* npm-run-all
* moment
* uuid
* passport
* jsonwebtoken
* passport-jwt
* bcrypt-nodejs

### www

The frontend will be built in React. The packages utilized will include:

* react-router-dom
* redux
* react-redux
* material-ui
* material-table
* formik
* formik-material-ui
* yup
* request

### Further Instructions

Each submodule has its own README.md in their respective project-root directories. These README documents provide the instructions to get the individual projects to a running state.

Once all three submodules are in a running state, the whole system can be booted with ```npm start``` from this projects root directory.